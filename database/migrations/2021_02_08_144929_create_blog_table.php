<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_blog');
            $table->longText('content_blog')->nullable();
            $table->string('photo_blog')->nullable();
            $table->date('tgl_posting')->nullable();
            $table->string('slug')->nullable();
            $table->text('tags')->nullable(); 
            $table->bigInteger('id_category_blog')->unsigned();
            $table->foreign('id_category_blog')->references('id')->on('blog_category')->onDelete('cascade');
            $table->bigInteger('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('author')->onDelete('cascade');
            $table->bigInteger('id_admin')->unsigned();
            $table->foreign('id_admin')->references('id')->on('admins')->onDelete('cascade');
            $table->text('meta_title')->nullable(); 
            $table->text('meta_description')->nullable(); 
            $table->date('published_at')->nullable();
            $table->string('status_draft')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
