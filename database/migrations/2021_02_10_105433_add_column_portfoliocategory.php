<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPortfoliocategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portofolio', function (Blueprint $table) {
            $table->bigInteger('portofolio_categories_id')->unsigned()->nullable();
            $table->foreign('portofolio_categories_id')->references('id')->on('portofolio_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
