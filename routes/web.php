<?php

use Illuminate\Support\Facades\Route;

//Auth
use App\Http\Controllers\Auth\AdminAuthController;

//Backend
use App\Http\Controllers\Backend\BlogController;
use App\Http\Controllers\Backend\BlogCommentController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\BlogCategoryController;
use App\Http\Controllers\Backend\AuthorController;
use App\Http\Controllers\Backend\PortofolioController;
use App\Http\Controllers\Backend\PortofolioCategoryController;
use App\Http\Controllers\Backend\ContactController;
use App\Http\Controllers\Backend\ServicesController;
use App\Http\Controllers\Backend\AboutController;
use App\Http\Controllers\Backend\QuotationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Backend\ProfilController;
use App\Http\Controllers\Backend\AdminController;
//Frontend
use App\Http\Controllers\Frontend\GeneralController;

//Media
use App\Http\Controllers\Media\MediaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Frontend
Route::get('/', [GeneralController::class, 'index']);
Route::get( '/portofolio', [GeneralController::class, 'portofolio'])->name('portofolio');
Route::get( '/portofolio/{slug}', [GeneralController::class, 'detailPortofolio'])->name('detailPortofolio');
Route::get( '/portofolio/category/{slug}', [GeneralController::class, 'categoryPortofolio'])->name('categoryPortofolio');
Route::get( '/searchportofolio', [GeneralController::class, 'searchPortofolio'])->name('searchPortofolio');
Route::post('/postquotation', [GeneralController::class, 'postQuotation']);
Route::get('/quotation', [GeneralController::class, 'getQuotation']);
Route::post('/contact', [GeneralController::class, 'postContact']);
Route::get('/contact', [GeneralController::class, 'getContact']);
Route::get('/services', [GeneralController::class, 'services']);
Route::post('/registerconsultation', [GeneralController::class, 'registerConsultation']);
Route::get('/about', [GeneralController::class, 'about']);

// Route::get( 'blog', [GeneralController::class, 'blog'])->name('blog');
// Route::get( '/blog/{slug}', [GeneralController::class, 'detailBlog']);
// Route::get( '/blog/category/{slug}', [GeneralController::class, 'categoryBlog'] )->name('categoryBlog');
// Route::get( '/blog/tag/{slug}', [GeneralController::class, 'tagBlog'])->name('tagBlog');


Route::get( 'blog', [GeneralController::class, 'blog'])->name('blog');
Route::get( 'terms', [GeneralController::class, 'termsCondition'] )->name('termsCondition');
Route::get( 'privacy', [GeneralController::class, 'privacyPolicy'] )->name('privacyPolicy');

Route::get('admin/login', [AdminAuthController::class, 'getLogin'])->name('admin.login');
Route::post('admin/login', [AdminAuthController::class, 'postLogin']);

// Backend
Route::prefix('admin')->group(function () {
    Route::middleware('auth:admin')->group(function(){
        Route::get('/dashboard', [DashboardController::class, 'index']);
        Route::get('/', [HomeController::class, 'index'])->name('home');
        
        Route::resource('quotation', QuotationController::class);
        Route::get('/quotation', [QuotationController::class, 'index']);
        Route::get('/quotation/{id}/view', [QuotationController::class, 'view']);
        
        //Blog
        Route::resource('blog', BlogController::class);
        Route::get('/blog', [BlogController::class, 'index']);
        Route::get('/blog/{id}/view', [BlogController::class, 'view']);
        Route::get('/ratingblog', [BlogController::class, 'ratingblog']);
        Route::get('/ratingblog/{id}/view', [BlogController::class, 'ratingview']);
        Route::resource('commentblog', BlogCommentController::class);
       //Author
        Route::resource('/author', AuthorController::class);
        Route::get('/author', [AuthorController::class, 'index']);
        Route::get('/author/{id}/view', [AuthorController::class, 'view']);
    
        //Category Blog
        Route::resource('/categoryblog', BlogCategoryController::class);
        Route::get('/categoryblog', [BlogCategoryController::class, 'index']);

        //About
        Route::resource('/about', AboutController::class);
        Route::get('/about', [AboutController::class, 'index']);

        //Contact Us
        Route::resource('/contact', ContactController::class);
        Route::get('/contact', [ContactController::class, 'index']);
        Route::get('/contact/{id}/view', [ContactController::class, 'view']);
         //Admin
         Route::resource('/admin', AdminController::class);
         Route::get('/admin', [AdminController::class, 'index']);
 
         Route::resource('/profile', ProfilController::class);
         Route::post('/update_profile', [ProfilController::class, 'update'] )->name('update.profile');
        //Portofolio
        Route::resource('/portofolio', PortofolioController::class);
        Route::get('/portofolio', [PortofolioController::class, 'index']);
        Route::get('/portofolio/{id}/view', [PortofolioController::class, 'view']);
        //Category Portofolio
        Route::resource('/portofoliocategory', PortofolioCategoryController::class);
        Route::get('/portofoliocategory', [PortofolioCategoryController::class, 'index']);

             //Service
        Route::resource('/services', ServicesController::class);
        Route::get('/services', [ServicesController::class, 'index']);

        Route::get('/logout', [AdminAuthController::class, 'postLogout']);
  });
});
Route::domain('news')->group(function () {        
  Route::get('/', [MediaController::class, 'index'])->name('news');
  Route::get('/news', [MediaController::class, 'index'])->name('news');
  Route::get( '/news/{slug}', [MediaController::class, 'detailBlog']);
  Route::get( '/news/category/{slug}', [MediaController::class, 'categoryBlog'] )->name('categoryBlog');
  Route::get( '/news/tag/{slug}', [MediaController::class, 'tagBlog'])->name('tagBlog');
  Route::get( '/news/searchblog', [MediaController::class, 'searchBlog'] )->name('news.searchBlog');
  Route::post('/news/ratingblog', [MediaController::class, 'ratingblog']);     
});

Route::get( '/searchblog', [MediaController::class, 'searchBlog'] )->name('searchBlog');