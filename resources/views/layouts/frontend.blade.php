<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Digital Marketing Home Page</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('image/logosingle.png') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/flaticon.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/slick-theme.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/royal-preload.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <script data-ad-client="ca-pub-6963936343766540" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script id="dsq-count-scr" src="//campaignlab.disqus.com/count.js" async></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
</head>

<body class="royal_preloader">
    <div id="page" class="site">
    @include('frontend.components.header')
    <!-- Header -->
    @yield('content') 
    @include('frontend.components.footer')
<a href="https://wa.me/628113435333?text=Halo%2C+saya+tertarik+dengan+Campaign+Lab" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>
        
</div><!-- #page -->
<a id="back-to-top" href="#" class="show"><i class="flaticon-arrow-pointing-to-up"></i></a>
        <!-- jQuery -->
    <script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/slick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('frontend/js/easypiechart.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('frontend/js/particles.min.js') }}"></script>
    <script src="{{ asset('frontend/js/scripts.js') }}"></script>
    <script src="{{ asset('frontend/js/header-mobile.js') }}"></script>
    <script src="{{ asset('frontend/js/royal_preloader.min.js') }}"></script>
    <script src="{{ asset('js/share.js') }}"></script>
</body>
</html>
<script type="text/javascript">
    window.jQuery = window.$ = jQuery;  
    (function($) { "use strict";
        //Preloader
        Royal_Preloader.config({
            mode           : 'logo',
            logo           : "{{ asset('image/logosingle2.png') }}",
            logo_size      : [100, 100],
            showProgress   : true,
            showPercentage : true,
            text_colour: '#000000',
            background:  '#ffffff'
        });
    })(jQuery);
</script> 