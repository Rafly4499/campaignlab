<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <!-- Basic metas
		======================================== -->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- Mobile specific metas
		======================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Papr">
    <meta name="twitter:creator" content="@Papr">
    <meta name="twitter:title" content="Papr">
    <meta name="twitter:description" content="Papr Trendy News and Megazine Template">
    <meta name="twitter:image" content="{{ asset('image/logomain.png') }}">
    <!-- Facebook -->
    <meta property="og:url" content="http://axilthemes.com/demo/template/papr">
    <meta property="og:title" content="Papr">
    <meta property="og:description" content="Papr Trendy News and Megazine Template">
    <meta property="og:type" content="website">
    <meta property="og:image" content="assets/images/papr.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <!-- Page Title
		======================================== -->
    <title>Media Campaign Lab</title>
    <!-- links for favicon
		======================================== -->
    <link rel="apple-touch-icon" type="image/png" href="{{ asset('image/logosingle.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('image/logosingle.png') }}">

    <link rel="manifest" href="{{ asset('media/assets/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('image/logosingle.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- Icon fonts
		======================================== -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,500i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/iconfont.css') }}">
    <!-- css links
		======================================== -->
    <!-- Bootstrap link -->
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/vendor/bootstrap.min.css') }}">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="{{ asset('media/assets/css/vendor/owl.carousel.min.css') }}">
    <!-- Slick slider -->
    <link rel="stylesheet" href="{{ asset('media/assets/css/vendor/slick.css') }}">
    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/vendor/magnific-popup.css') }}">
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/vendor/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/
    .css') }}">
    <!-- Plyr css -->

    <!-- Custom css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/style.css') }}">
    
    <link rel="stylesheet" type="text/css" href="{{ asset('media/assets/css/custom.css') }}">
    <script data-ad-client="ca-pub-6963936343766540" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script id="dsq-count-scr" src="//campaignlab.disqus.com/count.js" async></script>
</head>

<body>
    <div class="main-content">
        @include('media.components.header')
        @yield('content') 
        @include('media.components.footer')
    </div>
    <!-- End of .main-content -->
    <!-- Javascripts
		======================================= -->
    <!-- jQuery -->
    <script src="{{ asset('media/assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('media/assets/js/vendor/jquery-migrate.min.js') }}"></script>
    <!-- jQuery Easing Plugin -->
    <script src="{{ asset('media/assets/js/vendor/easing-1.3.js') }}"></script>
    <!-- Waypoints js -->
    <script src="{{ asset('media/assets/js/vendor/jquery.waypoints.min.js') }}"></script>
    <!-- Owl Carousel JS -->
    <script src="{{ asset('media/assets/js/vendor/owl.carousel.min.js') }}"></script>
    <!-- Slick Carousel JS -->
    <script src="{{ asset('media/assets/js/vendor/slick.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('media/assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('media/assets/js/vendor/isotope.pkgd.min.js') }}"></script>
    <!-- Counter up js -->
    <script src="{{ asset('media/assets/js/vendor/jquery.counterup.js') }}"></script>
    <!-- Magnific Popup js -->
    <script src="{{ asset('media/assets/js/vendor/jquery.magnific-popup.min.js') }}"></script>
    <!-- Nicescroll js -->
    <script src="{{ asset('media/assets/js/vendor/jquery.nicescroll.min.js') }}"></script>
    <!-- IF ie -->
    <script src="https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2"></script>
    <!-- Plugins -->
    <script src="{{ asset('media/assets/js/plugins.js') }}"></script>
    <!-- Custom Script -->
    <script src="{{ asset('media/assets/js/main.js') }}"></script>
    <script src="{{ asset('js/share.js') }}"></script>
</body>
</html>