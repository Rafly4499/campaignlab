<header id="site-header" class="site-header header-style-3 sticky-header header-fullwidth">
    <!-- Main header start -->
        <div class="octf-main-header">
            <div class="octf-area-wrap">
                <div class="container-fluid octf-mainbar-container">
                    <div class="octf-mainbar">
                        <div class="octf-mainbar-row octf-row">
                            <div class="octf-col">
                                <div id="site-logo" class="site-logo">
                                    <a href="index.html">
                                        <img class="logo-static" src="{{ asset('image/logomain.png') }}" alt="Onum">
                                    </a>
                                </div>
                            </div>
                            <div class="octf-col">
                                <nav id="site-navigation" class="main-navigation">           
                                    <ul id="primary-menu" class="menu">
                                        <li class="current-menu-item current-menu-ancestor"><a href="/" >Home</a></li>
                                        <li><a href="{{ url('/services') }}">Services</a>
                                           
                                        </li>
                                        <li><a href="https://campaignlab.co.id">Blog</a>
                                        </li>
                                        <li><a href="{{ url('/quotation') }}">Start Business</a>
                                        </li>
                                        <li><a href="{{ url('/contact') }}">Contacts</a>
                                        </li>
                                    </ul>                             
                                </nav><!-- #site-navigation -->
                            </div>
                            <div class="octf-col text-right">
                                <!-- Call To Action -->
                                <div class="octf-btn-cta">
                                    <div class="octf-header-module">
                                    <div class="btn-cta-group btn-cta-header">
                                        <a class="octf-btn octf-btn-third" href="https://wa.me/628113435333?text=Halo%2C+saya+tertarik+dengan+Campaign+Lab">Contact Us Via WA</a>
                                    </div>
                                </div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
        <!-- Main header close -->

    <!-- Header mobile open -->
    <div class="header_mobile">
        <div class="container">
            <div class="mlogo_wrapper clearfix">
                <div class="mobile_logo">
                    <a href="index.html"><img src="{{ asset('image/logomain.png') }}" alt="Onum" data-lazy-src="{{ asset('image/logomain.png') }}">
                        <noscript><img src="{{ asset('image/logomain.png') }}" alt="Onum"></noscript>
                    </a>
                </div>
                <div id="mmenu_toggle">
                    <button></button>
                </div>
            </div>
            <div class="mmenu_wrapper">     
                <div class="mobile_nav">
                    <ul id="menu-main-menu" class="mobile_mainmenu">
                        <li class="current-menu-item current-menu-ancestor"><a href="/" >Home</a></li>
                                        <li><a href="{{ url('/services') }}">Services</a>
                                        </li>
                                        <li><a href="{{ url('/blog') }}">Blog</a>
                                        </li>
                                        <li><a href="{{ url('/quotation') }}">Start Business</a>
                                        </li>
                                        <li><a href="{{ url('/contact') }}">Contacts</a>
                                        </li>
                    </ul>        
                </div>      
            </div>
        </div>
    </div>
    <!-- Header mobile close -->

</header>