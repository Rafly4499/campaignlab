<footer id="site-footer-home2" class="site-footer-home2 site-footer-2 site-footer-home4">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 xs-text-center">
                    <img style="width:124px;" src="{{ asset('image/logowhite.png') }}" alt="">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="copyright-2 text-right">Copyright © 2021 Campaign Lab. All Rights Reserved.</p>
                </div>
                
               
            </div>
        </div>
    </div>
</footer><!-- #site-footer -->