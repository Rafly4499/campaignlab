@extends('layouts.frontend')

@section('content')

<div id="content" class="site-content">
            <section class="home4-top">
                <div class="container">
                    <div class="flex-row">
                        
                        <div class="home4-top-right align-self-center">
                            <h6>our experience boost your business</h6>
                            <h2>Perluas Jangkauan Bisnis Anda Dengan Promosi Tepat Sasaran</h2>
                            <p>Jangan buang waktu anda untuk sebuah program promosi yang monoton dan tidak terukur efektifitasnya. <br/>
                                Tidak Mau, kan ?
                                </p>
                            <div class="ot-button">
                                <a href="{{ url('/contact')}}" class="octf-btn octf-btn-primary octf-btn-icon"><span>Yuk, Optimalkan Sekarang  <i class="flaticon-right-arrow-1"></i></span></a>
                            </div>
                        </div>
                        <div class="home4-top-left">
                            <div class="home4-top-right-img">
                                <img src="{{ asset('frontend/images/header-home3.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="h4-social-icons">
                    <a class="social-icon" href="#" target="_blank"><i class="fab fa-twitter"></i></a>
                    <a class="social-icon" href="#" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a class="social-icon" href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                    <a class="social-icon" href="#" target="_blank"><i class="fab fa-instagram"></i></a>
                </div>
            </section>

            <section class="home5-results particles-js" data-color="#fe4c1c,#00c3ff,#0160e7" data-id="i5-3">
                <div class="bg-overlay"></div>
                <div class="container">
                    <div class="flex-row">
                        <div class="home5-results-block col-md-6 col-sm-12 col-xs-12 sm-m-b40 align-self-center">
                            <div class="ot-heading left-align">
                                <h6><span>We get results</span></h6>
                                <h2 class="main-heading">Tahukah Kamu</h2>
                            </div>
                            <div class="ot-tabs">
                                {{-- <ul class="tabs-heading unstyle">
                                    <li class="tab-link octf-btn current" data-tab="tab-1454">Intergration</li>
                                    <li class="tab-link octf-btn" data-tab="tab-2454">Development</li>
                                </ul> --}}
                                <div id="tab-1454" class="tab-content current">
                                    
                                    <ul class="has-icon unstyle">
                                        <li><i class="flaticon-check"></i> Banyak perusahaan yang bingung harus mulai dari mana untuk berpromosi yang benar.                                        </li>
                                        <li><i class="flaticon-check"></i> Brand yang bagus, belum jadi tentu juga didukung program promosi yang tepat sasaran.                                        </li>
                                        <li><i class="flaticon-check"></i> Bahwa banyak juga yang bingung tentang Digital Marketing, Instagram Marketing, dan kapan harus menggunakan Website dan Landing Page
                                        </li>
                                        <li><i class="flaticon-check"></i> Bahkan, banyak website perusahaan yang hanya sekedar Company Profile saja dan tidak optimal membantu penjualan ?

                                        </li>
                                        <li><i class="flaticon-check"></i> Pasti bikin pusing, ya kan ?


                                        </li>
                                    </ul>
                                    <p>Kalau itu yang kamu hadapi, kamu berada di tempat yang tepat. Karena CampaignLab, akan membantu promosi bisnismu.
                                    </p>
                                    <p>Promosi Media Sosial? Optimasi Digital? Atau Promosi Media Konvensional? Kami bantu.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="home5-results-img col-md-6 col-sm-12 col-xs-12 text-center align-self-center">
                            <img src="{{ asset('frontend/images/topimg-home1.png') }}">
                        </div>
                    </div>
                </div>
            <div class="onum-particles" id="particles-i5-3"><canvas class="particles-js-canvas-el" width="1903" height="819" style="width: 100%; height: 100%;"></canvas></div></section>

            <section class="home4-about">
                <div class="container">
                    <div class="flex-row">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="home4-about-left">
                                <div class="ot-heading left-align">
                                    <h6><span>about us</span></h6>
                                    <h2 class="main-heading">Campaign Lab</h2>
                                </div>
                                <p class="font22 text-dark">The A-Z Campaign Story
                                </p>
                                <p class="m-b18">CampaignLab merupakan digital marketing agency, yang akan membantu setiap bisnis tampil prima di hadapan pelanggan. Promosi digital maupun konvensional, CampaignLab akan siap membantu bisnismu tampil cemerlang. Tak hanya itu, kami akan membantumu mempermudah pelayanan pelanggan melalui berbagai optimalisasi platform digital
                                </p> 
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="home4-about-right">
                                
                                <img src="{{ asset('frontend/images/image1-home4.png') }}" alt>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="home4-process p-b110">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 md-m-b30">
                            <div class="service-box s-box">
                                <div class="overlay"></div>
                                <span class="big-number">01</span>
                                <div class="icon-main number-box"><span class="flaticon-pie-chart-1"></span></div>
                                <div class="content-box">
                                    <h5>Digital Branding</h5>
                                    <p>Visualkan brand anda, mulai dari LOGO yang sederhana tapi penuh makna.
                                        Rancang sekarang!
                                        </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 md-m-b30">
                            <div class="service-box s-box">
                                <div class="overlay"></div>
                                <span class="big-number">02</span>
                                    <div class="icon-main number-box"><span class="flaticon-document"></span></div>
                                <div class="content-box">
                                    <h5>Online Campaign
                                    </h5>
                                    <p>Rancang, susun, buat content mapping, pilih channel media sosial yang paling berpengaruh, mulai dari Instagram, Facebook, dan lainnya.
                                        Kelola Posting!
                                        </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 md-m-b30">
                            <div class="service-box s-box">
                                <div class="overlay"></div>
                                <span class="big-number">03</span>
                                    <div class="icon-main number-box"><span class="flaticon-search-1"></span></div>
                                <div class="content-box">
                                    <h5>Offline Campaign
                                    </h5>
                                    <p>Kombinasikan juga kampanye dengan promosi di channel konvensional yang ada.
                                        Radio, koran, dan lainnya.
                                        Atur iklanmu!
                                        </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="service-box s-box elementor-animation-float">
                                <div class="overlay"></div>
                                <span class="big-number">04</span>
                                    <div class="icon-main number-box"><span class="flaticon-pie-chart"></span></div>
                                <div class="content-box">
                                    <h5>Digital Optimalization
                                    </h5>
                                    <p>Digitalisasikan bisnismu, agar engagement terus terjaga.
                                        Rancang apps-mu, ecommerce-mu, dan kombinasikan digital platform lainnya.
                                        Optimalkan bisnismu                                        
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="home4-partners p-b110">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="ot-heading text-center" style="margin-bottom: 40px;">
                                <h6><span>our partner</span></h6>
                                <h3 class="main-heading">Mereka yang pernah bermitra dengan kami</h3>
                            </div>
                        </div>
                    </div>
                    <div class="partners">
                        <div class="owl-carousel home-client-carousel">
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/1.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/2.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/3.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/4.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/5.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/6.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/7.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/8.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/9.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/10.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/11.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/12.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/13.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                            <div class="partners-slide">
                                <a href="#" class="client-logo">
                                    <figure class="partners-slide-inner">
                                        <img class="partners-slide-image" src="{{ asset('frontend/images/partner/14.png') }}" alt="">
                                    </figure>                             
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="home4-testimonial">
                <div class="container">
                    <div class="flex-row">
                        <div class="col-md-8 col-sm-12 col-xs-12 align-self-center">
                            <div class="home4-testimonial-left">
                                <div class="ot-heading left-align">
                                    <h6><span>real testimonials</span></h6>
                                    <h2 class="main-heading">What They Say About Our Company?</h2>
                                </div>
                                
                                <div class="ot-testimonials">
                                    <div class="slider__arrows"></div>
                                    <div class="testimonial-wrap">
                                        <div class="testimonial-inner ot-testimonials-slider">
                                            <div>
                                                <img src="{{ asset('frontend/images/testi1.png') }}" alt="Michael Terry">
                                                <div class="ttext">
                                                    Campaign Lab has increased our traffic, keywords, and conversion. We’ve enjoyed working with them and consider them a strategic business partner.
                                                </div>
                                                <div class="tinfo">
                                                    <h6>Michael Terry</h6>
                                                    <span>Developer</span> 
                                                </div>
                                            </div>
                                            <div>
                                                <img src="{{ asset('frontend/images/testi2.png') }}" alt="Emilia Clarke">
                                                <div class="ttext">
                                                    I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.
                                                </div>
                                                <div class="tinfo">
                                                    <h6>Emilia Clarke</h6>
                                                    <span>Manager</span> 
                                                </div>
                                            </div>
                                            <div>
                                                <img src="{{ asset('frontend/images/testi4.png') }}" alt="Cristian Torres">
                                                <div class="ttext">
                                                    I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. Thank you!
                                                </div>
                                                <div class="tinfo">
                                                    <h6>Cristian Torres</h6>
                                                    <span>Designer</span> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-block"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="home4-testimonial-right">
                                <img src="{{ asset('frontend/images/img-services-3.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="p-t330 p-b210 cta bg-cta particles-js" data-color="#fe4c1c,#00c3ff,#0160e7" data-id="i1-1">
                <div class="bg-overlay opacity-1"></div>
                <div class="shape shape-top" data-negative="false">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                        <path class="shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"></path>
                    </svg>
                </div>
                <div class="container">
                    <div class="cta-content text-center">
                        <h2 class="m-b20">Ingat, bisnis sangatlah dinamis. <br>Trend terus berubah.
                        </h2>
                        <p class="m-b35">Make the Right Choice for Your Future. Choose Campaign Lab!</p>
                    </div>
                </div>
            <div class="onum-particles" id="particles-i1-1"><canvas class="particles-js-canvas-el" width="1903" height="852" style="width: 100%; height: 100%;"></canvas></div></section>
            
            
            {{-- <section class="seo-about p-b30">
                <div class="shape shape-bottom" data-negative="false">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                        <path class="shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"></path>
                    </svg>
                </div>
            </section> --}}
            <section class="p-t35 p-b90 xs-p-t60 xs-p-b60 bg-about">
                <div class="container">
                    <div class="row flex-row">
                        <div class="col-lg-6 col-md-12 col-xs-12 xs-p-b50 text-center">
                            <img src="{{ asset('frontend/images/img-services-1.png') }}" class="" alt="onum">
                        </div>
                        <div class="col-lg-6 col-md-12 col-xs-12 p-l95 xs-p-l15 p-r0 xs-p-r15 align-self-center">
                            <div class="ot-heading left-align">
                                <h6><span>Consultant</span></h6>
                                <h2 class="main-heading m-b20">Get Your Free Consultation</h2>
                            </div>
                            <p class="font22 text-dark">Bisnis anda adalah masa depan anda. </p>
                            <p class="m-b0 p-b45">Tak khayal, sebagai bagian penting dalam perusahaan, anda memiliki peran yang sangat penting didalamnya.
                                <br>Promosi anda, baik offline maupun online, mungkin menjadi rumit bagi anda. Silahkan manfaatkan kesempatan berkonsultasi dengan kami, sekarang.
                            </p>
                            <div class="ot-button">
                                <a href="{{ url('/contact')}}" class="octf-btn octf-btn-primary octf-btn-icon"><span>Daftar sekarang untuk konsultasi
                                    <i class="flaticon-right-arrow-1"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        @endsection