@extends('layouts.frontend')

@section('content')

<div id="content" class="site-content">
    <div class="page-header dtable text-center" style="background-image: url({{ asset('frontend/images/bg-page-header.jpg);') }}">
        <div class="dcell">
            <div class="container">
                <h1 class="page-title">Contacts</h1>
                <ul id="breadcrumbs" class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li class="active">Contacts</li>
                </ul>
            </div>
        </div>
    </div>

    <section class="p-t110 z-index-1 section-contact">
        <div class="container">
            <div class="row flex-row">
                @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Success!',
                        text:  'Register anda telah berhasil. silahkan menghubungi lebih lanjut melalui WhatsApp.',
                        icon: 'success',
                        confirmButtonText: '<a style="color: white;" href="https://wa.me/628113435333?text=Halo%2C+saya+ingin+konsultasi+bisnis+dengan+Campaign+Lab">WhatsApp Campaign Lab</a>'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Failed!',
                        text:  'Your register failed. Check Your Field Input',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                <div class="col-md-6 col-xs-12 sm-m-b60">
                    <div class="our-contact text-light">
                        <div class="ot-heading text-light">
                            <h6><span>contact details</span></h6>
                            <h2 class="main-heading">Our Contacts </h2>
                        </div>
                        <p class="m-b45">Give us a call or drop by anytime, we endeavour to answer all enquiries within 24 hours on business days. We will be happy to answer your questions.</p>
                        <div class="contact-info text-light m-b30">
                            <i class="flaticon-world"></i>
                            <div class="info-text">
                                <h6>Our Address:</h6>
                                <p>Jl. Barito 11 Surabaya, Jawa Timur, Indonesia</p>
                            </div>
                        </div>
                        <div class="contact-info text-light m-b30">
                            <i class="flaticon-note"></i>
                            <div class="info-text">
                                <h6>Our mailbox:</h6>
                                <p style="font-size: 20px;"><a href="mailto:cs@campaignlab.co.id">cs@campaignlab.co.id</a></p>
                                <p style="font-size: 20px;"><a href="mailto:campaignlab.indonesia@gmail.com">campaignlab.indonesia@gmail.com</a></p>
                            </div>
                        </div>
                        <div class="contact-info text-light">
                            <i class="flaticon-viber"></i>
                            <div class="info-text">
                                <h6>Our phones:</h6>
                                <p><a href="tel:+62 811-3435-333">+62 811-3435-333</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 align-self-center">
                    <div class="form-contact">
                        <div class="ot-heading">
                                <h6><span>GET IN TOUCH</span></h6>
                            <h2 class="main-heading">Get Your Free Consultation</h2>
                        </div>
                        <form class="wpcf7-form"  method="post" action="{{ url('/registerconsultation') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <p>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" name="name" id="wa_name" class="wpcf7-form-control wpcf7-text" placeholder="Your Name *" required="">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="email" id="wa_email" class="wpcf7-form-control wpcf7-text wpcf7-email" placeholder="Your Email *" required="">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="number" name="no_hp" id="wa_number" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Your Phone *" required="">
                                </span>
                            </p>
                            <p>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="text" name="company" id="wa_company" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Your Company *" required="">
                                </span>
                            </p>
                            <p>
                                <button type="submit" class="octf-btn octf-btn-primary octf-btn-icon">Slahkan Daftar untuk Free Consultation
                                    <i class="flaticon-right-arrow-1"></i></button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d989.3912622375551!2d112.7326668!3d-7.2902254!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fb9488b31331%3A0x134294a8906d5284!2sJl.%20Barito%20No.11%2C%20RT.005%2FRW.01%2C%20Darmo%2C%20Kec.%20Wonokromo%2C%20Kota%20SBY%2C%20Jawa%20Timur%2060241!5e0!3m2!1sen!2sid!4v1612977550047!5m2!1sen!2sid" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </section>
</div>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script>
    $(document).on('click','.send_form', function(){
var input_blanter = document.getElementById('wa_email');

/* Whatsapp Settings */
var walink = 'https://web.whatsapp.com/send',
    phone = '6288217469934',
    walink2 = 'Halo saya ingin mendaftar free konsultasi dengan Campaign Lab',
    text_yes = 'Terkirim.',
    text_no = 'Isi semua Formulir lalu klik Send.';

/* Smartphone Support */
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
var walink = 'whatsapp://send';
}

if("" != input_blanter.value){

 /* Call Input Form */
var input_name1 = $("#wa_name").val(),
    input_email1 = $("#wa_email").val(),
    input_number1 = $("#wa_number").val(),
    input_company = $("#wa_company").val();

/* Final Whatsapp URL */
var blanter_whatsapp = walink + '?phone=' + phone + '&text=' + walink2 + '%0A%0A' +
    '*Name* : ' + input_name1 + '%0A' +
    '*Email Address* : ' + input_email1 + '%0A' +
    '*Input Number* : ' + input_number1 + '%0A' +
    '*Company* : ' + input_company + '%0A';
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });// this prevents the form from submitting
      $.ajax({
        url: '/registerconsultation',
        method: 'post',
        data: $('#form-id').serialize(), // prefer use serialize method
        success:function(data){
            console.log(data);          
        }
      });
/* Whatsapp Window Open */
window.open(blanter_whatsapp,'_blank');
document.getElementById("text-info").innerHTML = '<span class="yes">'+text_yes+'</span>';
} else {
document.getElementById("text-info").innerHTML = '<span class="no">'+text_no+'</span>';
}
});
</script> --}}
        @endsection