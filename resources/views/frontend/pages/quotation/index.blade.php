@extends('layouts.frontend')

@section('content')

<div id="content" class="site-content">
    <div class="page-header dtable text-center" style="background-image: url({{ asset('frontend/images/bg-page-header.jpg);') }}">
        <div class="dcell">
            <div class="container">
                <h1 class="page-title">Start Business</h1>
                <ul id="breadcrumbs" class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li class="active">Start Business</li>
                </ul>
            </div>
        </div>
    </div>

    <section class="p-t110 p-b110 z-index-1 section-contact">
        <div class="container">
            <div class="row flex-row">
                @if ($message = Session::get('success'))
                <script>
                    Swal.fire({
                        title: 'Success!',
                        text:  'Your Request contact Success. CampaignLab will contact you shortly.',
                        icon: 'success',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                @if ($message = Session::get('error'))
                <script>
                    Swal.fire({
                        title: 'Failed!',
                        text:  'Your register failed. Check Your Field Input',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                      })
                    </script>
                @endif
                <div class="col-md-12 col-xs-12 sm-m-b60">
                    <div class="our-contact text-light">
                        <div class="ot-heading text-light">
                            <h6><span>Start Business Details</span></h6>
                            <h2 class="main-heading">Start Your Business With Campaign Lab. </h2>
                            <p class="m-b45">Survive the new normal. Market your business better during pandemic. Please complete the form below and we will get back to you in one or two business days.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 align-self-center" style="margin-top: 40px;">
                    <div class="form-contact">
                        <div class="ot-heading">
                                <h6><span>Quotation</span></h6>
                            <h2 class="main-heading">Your Planner</h2>
                        </div>
                        <div class="row mt-4"">
                        <form class="wpcf7-form"  method="post" action="{{ url('/postquotation') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">WHAT SHOULD WE CALL YOU?</label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" name="name" class="wpcf7-form-control wpcf7-text" placeholder="Your Name *" required="">
                                </span>
                            </p>
                            </div>
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">WHAT IS YOUR COMPANY/ORGANISATION NAME?</label>
                                <span class="wpcf7-form-control-wrap your-name">
                                    <input type="text" name="company" class="wpcf7-form-control wpcf7-text" placeholder="Your Company *" required="">
                                </span>
                            </p>
                            </div>
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">WHAT IS YOUR EMAIL ADDRESS?*</label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="email" name="email" class="wpcf7-form-control wpcf7-text wpcf7-email" placeholder="Your Email *" required="">
                                </span>
                            </p>
                            </div>
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">WHAT IS YOUR PHONE NUMBER?</label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="number" name="no_hp" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Your Phone *" required="">
                                </span>
                            </p>
                            </div>
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">WHAT CAN WE HELP YOU WITH?</label> <br>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <select name="project" class="selectproject quotationselect" required>
                                        <option>Select Services</option>
                                        <option value="Digital Branding">Digital Branding</option>
                                        <option value="Online Campaign">Online Campaign</option>
                                        <option value="Offline Campaign">Offline Campaign</option>
                                        <option value="Digital Optimization">Digital Optimization</option>
                                        <option value="All Services">All Services</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </span>
                            </p>
                            </div>
                            <div class="col-md-6 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p class="other_project">
                                 <label for="" style="font-size: 12px;">OTHER SERVICES YOU WANT</label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <input type="text" name="other_project" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Other Services" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Other Services'">
                                </span>
                            </p>
                            </div>
                            <div class="col-md-12 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">DESCRIBE THE PROJECT, TIMELINE, AND BUDGET*</label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <textarea name="description" id="description" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Requirements, target audience, goals, timeline, budget, etc." required></textarea>
                                </span>
                            </p>
                            </div>
                            <div class="col-md-12 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <label for="" style="font-size: 12px;">REFERENCES</label>
                                <span class="wpcf7-form-control-wrap your-email">
                                    <textarea name="references" id="description" class="wpcf7-form-control wpcf7-text wpcf7-text" placeholder="Key visuals you like (or dislike) in websites, social media accounts, etc."></textarea>
                                </span>
                            </p>
                            </div>
                            <div class="col-md-12 col-xs-12 mt-4" style="margin-top: 15px;">
                            <p>
                                <button type="submit" class="octf-btn octf-btn-primary octf-btn-icon">Submit Planner
                                    <i class="flaticon-right-arrow-1"></i></button>
                            </p>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script>
      $(document).ready(function () {
        $(".other_project").css("display", "none");
          $('select[name=project]').on('change', function(){
        var departmentId = this.value;
         if (departmentId == "Other") {
               $(".quotationselect");
               $(".other_project").css("display", "block");
               $(".other_project").find('input').attr('required', 'required');
         } else {
               $(".quotationselect");
               $(".other_project").css("display", "none");
               $(".other_project").find('input').removeAttr('required');
         } 
     });
    });
    </script>
        @endsection