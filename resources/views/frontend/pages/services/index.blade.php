@extends('layouts.frontend')

@section('content')
<div id="content" class="site-content">
    <div class="page-header dtable text-center" style="background-image: url({{ asset('frontend/images/bg-page-header.jpg);') }}">
        <div class="dcell">
            <div class="container">
                <h1 class="page-title">Our Services</h1>
                <ul id="breadcrumbs" class="breadcrumbs">
                    <li><a href="/">Home</a></li>
                    <li class="active">Our Services</li>
                </ul>
            </div>
        </div>
    </div>

    <section class="p-t100 p-b120 bg-process">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ot-heading text-center m-b80">
                        <h6><span>Organize Business</span></h6>
                        <h2 class="main-heading">Bagian mana yang sering membuatmu bingung ?</h2>
                    </div>
                </div>
            </div>
            <div class="flex-row justify-content-center">
                <div class="col-md-3 col-sm-6 col-xs-12 text-center sm-m-b40">
                    <div class="process-box">
                        <img class="p-arrow" src="{{ asset('frontend/images/p-arrow1.png') }}" alt="arrow">           
                        <div class="number-box">01</div>
                        <div class="icon-main">
                            <img src="{{ asset('frontend/images/process1.png') }}" alt="Project Introduction">
                        </div>
                        <div class="content-box">
                            <p>Mengelola Brand dan Visualisasi Bisnis anda?</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 text-center sm-m-b40">
                    <div class="process-box">
                        <img class="p-arrow" src="{{ asset('frontend/images/p-arrow2-1.png') }}" alt="arrow">
                        <div class="number-box">02</div>
                        <div class="icon-main p-b20">
                             <img src="{{ asset('frontend/images/process2-1.png') }}" alt="Research &amp; Concept">
                        </div>
                        <div class="content-box">
                            <p>Mengelola Promosi di Media Sosial?
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                    <div class="process-box">
                        <img class="p-arrow" src="{{ asset('frontend/images/p-arrow2-1.png') }}" alt="arrow">
                        <div class="number-box">03</div>
                        <div class="icon-main p-b20">
                            <img src="{{ asset('frontend/images/process3-1.png') }}" alt="Project Termination">
                        </div>
                        <div class="content-box">
                            <p>Mengelola Promosi Anda secara offline?
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                    <div class="process-box">
                        <div class="number-box">04</div>
                        <div class="icon-main p-b20">
                            <img src="{{ asset('frontend/images/process4.png') }}" alt="Project Termination">
                        </div>
                        <div class="content-box">
                            <p>Membuat dan Mengelola Website dan Aplikasi Digital?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="z-index-1">
        <div class="section-tab">
            <div class="container">
                <div class="flex-row align-items-end m-b20">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <div class="ot-heading text-light">
                            <h6><span>Build your business with Campaign Lab</span></h6>
                            <h2 class="main-heading">Raih kesuksesan bisnis anda melalui Campaign Lab</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="tab-titles row">
                            <div>
                                <div class="col-md text-center">
                                    <a class="title-item tab-active" href="#content-mkt" tabindex="0">
                                        <div class="icon-main"><span class="flaticon-pie-chart"></span></div>
                                        <h6>Digital<br> Branding</h6>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <div class="col-md text-center">
                                    <a class="title-item" href="#media-mkt" tabindex="0">
                                        <div class="icon-main"><span class="flaticon-pie-chart-1"></span></div>
                                        <h6>Online<br> Campaign</h6>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <div class="col-md text-center">
                                    <a class="title-item" href="#app-dev" tabindex="0">
                                        <div class="icon-main"><span class="flaticon-document"></span></div>
                                        <h6>Offline<br>Campaign</h6>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <div class="col-md text-center">
                                    <a class="title-item" href="#seo-opt" tabindex="0">
                                        <div class="icon-main"><span class="flaticon-search-1"></span></div>
                                        <h6>Digital<br> Optimization</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tab-slider-content" id="content-tabs">
        <div class="container">
            <div class="flex-row section-content" id="content-mkt">
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                    <div class="tab-slider-block">
                        <h2>Digital Branding</h2>
                        <p>Kalau Pelanggan Anda Belum Mengenal anda, mungkin bisnis anda belum memiliki identitas yang sesuai, maka pertajamlah identitas bisnis anda. 
                        </p>
                        <p class="m-b50">Rancang konsep logonya, buat standarisasi promosinya melalui Campaign Lab. Campaign Lab menyediakan fitur digital branding untuk memberikan branding dan visualisasi brand anda secara digital.
                        </p>
                        <div class="ot-button">
                            <a href="{{ url('/contact') }}" class="octf-btn octf-btn-secondary octf-btn-icon"><span>Konsultasikan secara gratis
                                <i class="flaticon-right-arrow-1"></i></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                    <div class="tab-slider-img">
                        <img src="{{ asset('frontend/images/img-services-1.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="flex-row section-content" id="media-mkt">
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                    <div class="tab-slider-block">
                        <h2>Online Campaign</h2>
                        <p>Kami akan bantu mengelola postingan anda, feed konten anda tertata rapi, supaya terjadi engagement dengan audiens anda.
                            Mulai dari content mapping, content design, hingga kelola posting kami bantu. 
                            </p>
                            <p class="m-b50">Campaign Lab menyediakan fitur online campaign dengan pengelolaan promosi di
                                channel online. </p>
                        <div class="ot-button">
                            <a href="{{ url('/contact') }}" class="octf-btn octf-btn-secondary octf-btn-icon"><span>Silahkan tanya sekarang.
                                <i class="flaticon-right-arrow-1"></i></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                    <div class="tab-slider-img">
                        <img src="{{ asset('frontend/images/whyus-home1.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="flex-row section-content" id="app-dev">
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                    <div class="tab-slider-block">
                        <h2>Offline Campaign</h2>
                        <p>Jangan sia-siakan online campaign anda begitu saja, sebab pelanggan anda pun tak selamanya dan tak selalu terkoneksi secara online.
                            Mereka pun mendengarkan radio, baca koran, lihat billboard, dan menonton televisi.
                            </p>
                        <p class="m-b50">Campaign Lab menyediakan fitur online campaign dengan pengelolaan promosi di
                            channel offline. </p>
                        <div class="ot-button">
                            <a href="{{ url('/contact') }}" class="octf-btn octf-btn-secondary octf-btn-icon"><span>Silahkan sampaikan masalah promosimu, sekarang.
                                <i class="flaticon-right-arrow-1"></i></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                    <div class="tab-slider-img">
                        <img src="{{ asset('frontend/images/image-banner-home6.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="flex-row section-content" id="seo-opt">
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                    <div class="tab-slider-block">
                        <h2>Digital Optimalization </h2>
                        <p>Bagaimanapun juga, yang anda butuhkan adalah interaksi dengan pelanggan. Ya kan ?
                            Tingkatkan konversi bisnis anda melalui platform digital yang anda punya. 
                            Buat Website dan Apps khusus untuk bisnis anda sekarang. 
                            </p>
                        <p class="m-b50">Campaign Lab menyediakan fitur Digital Optimalization yang bertujuan untuk mengoptimalisasikan platform bisnis anda melalui digital.</p>
                        <div class="ot-button">
                            <a href="{{ url('/contact') }}" class="octf-btn octf-btn-secondary octf-btn-icon"><span>Silahkan tanya sepuasnya.
                                <i class="flaticon-right-arrow-1"></i></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                    <div class="tab-slider-img">
                        <img src="{{ asset('frontend/images/img-services-3.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="p-t110 p-b90">
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-b20">
                    <div class="ot-heading text-center">
                        <h6><span>our projects</span></h6>
                        <h2 class="main-heading">View Some of Our Works <br>and Case Studies for Clients</h2>
                    </div>
                </div>
            </div>
            <div class="project-filter-wrapper">
                <div class="container">
                    <ul class="project_filters">
                        <li><a href="#" data-filter="*" class="selected">All</a></li>
                        <li><a href="#" data-filter=".branding">Digital Branding</a></li>                     
                        <li><a href="#" data-filter=".online">Online Campaign</a></li>                     
                        <li><a href="#" data-filter=".offline">Offline Campaign</a></li>                     
                        <li><a href="#" data-filter=".optimization">Digital Optimization</a></li>                       
                    </ul>
                </div>
                <div class="projects-grid">
                    <div class="project-item online optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/sosmed1.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Social Media</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item online optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/sosmed2.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Social Media</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/web1.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Website Development</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/web2.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Website Development</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/mobile1.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Mobile Development</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/mobile2.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Mobile Development</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/portfolio/mobile3.png') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Mobile Development</a></h5>
                                    <p class="portfolio-cates"><a href="#">Digital</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="project-item online optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-10.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">SMM Project</a></h5>
                                    <p class="portfolio-cates"><a href="#">Media</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item branding optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-11.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Responsive Design</a></h5>
                                    <p class="portfolio-cates"><a href="#">Optimization</a></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-12.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">App for Health</a></h5>
                                    <p class="portfolio-cates"><a href="#">Development</a></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item offline optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-6.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Basics Project</a></h5>
                                    <p class="portfolio-cates"><a href="#">Development</a><span>/</span><a href="#">Media</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item online optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-5.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Social Media App</a></h5>
                                    <p class="portfolio-cates"><a href="#">Marketing</a><span>/</span><a href="#">Media</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item offline">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-4.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Your New Reality</a></h5>
                                    <p class="portfolio-cates"><a href="#">Marketing</a><span>/</span><a href="#">Media</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item offline">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-3.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Immersive Experience</a></h5>
                                    <p class="portfolio-cates"><a href="#">Marketing</a></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item offline">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-2.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Cereal Project</a></h5>
                                    <p class="portfolio-cates"><a href="#">Development</a></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="project-item online optimization">
                        <div class="projects-box">
                            <div class="projects-thumbnail">
                                <a href="portfolio-single.html">
                                    <img src="{{ asset('frontend/images/projects/grid-1.jpg') }}" class="" alt="">
                                </a>
                            </div>
                            <div class="portfolio-info s1">
                                <div class="portfolio-info-inner">
                                    <h5><a href="portfolio-single.html">Crypto App Project</a></h5>
                                    <p class="portfolio-cates"><a href="#">Marketing</a><span>/</span><a href="#">Optimization</a><span>/</span></p> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
@endsection