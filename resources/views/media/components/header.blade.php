<header class="page-header">
    <nav class="navbar bg-grey-light-three navbar__style-three">
        <div class="container">
            <div class="navbar-inner justify-content-between">
                <div class="brand-logo-container">
                    <a href="index.html">
                        <img src="{{ asset('image/logomain.png') }}" alt="" class="brand-logo">
                    </a>
                </div>
                <!-- End of .brand-logo-container -->
                <div class="main-nav-wrapper">
                    <ul class="main-navigation list-inline" id="main-menu">
                        <li>
                            <a href="{{ url('/') }}">Home</a>
                            <!-- End of .submenu -->
                        </li>
                        
                        <li class="has-dropdown">
                            <a href="#">Services</a>
                            <ul class="submenu">
                                <li>
                                    <a href="https://campaignlab.co.id/content-marketing">Content Marketing</a>
                                </li>
                                <li>
                                    <a href="https://campaignlab.co.id/app-development">App Development</a>
                                </li>
                                <li>
                                    <a href="https://campaignlab.co.id/seo">Search Engine Optimization</a>
                                </li>
                                <li>
                                    <a href="https://campaignlab.co.id/management">PPC Management</a>
                                </li>
                                <li>
                                    <a href="https://campaignlab.co.id/marketing">Social Media Marketing</a>
                                </li>
                            </ul>
                            <!-- End of .submenu -->
                        </li>
                        <li class="is-active">
                            <a href="https://media.campaignlab.co.id">Blog</a>
                            <!-- End of .submenu -->
                        </li>
                        <li><a href="https://campaignlab.co.id/contact">Contacts</a></li>
                    </ul>
                    <!-- End of .main-navigation -->
                </div>
                <!-- End of .main-nav-wrapper -->
                <div class="navbar-extra-features">
                    <form action="{{ url('/searchblog') }}" class="navbar-search">
                        @csrf
                        <div class="search-field">
                            <input type="text" name="search" class="navbar-search-field" placeholder="Search Here..." value="{{ old('search') }}">
                            <button class="navbar-search-btn" type="button"><i class="fal fa-search"></i></button>
                        </div>
                        <!-- End of .search-field -->

                        <a href="#" class="navbar-search-close" style="margin-right: 2rem;"><i class="fal fa-times"></i></a>
                    </form>
                    <!-- End of .navbar-search -->
                    <a href="#" class="nav-search-field-toggler" data-toggle="nav-search-feild"><i class="far fa-search"></i></a>
                    <a class="octf-btn octf-btn-third" href="https://wa.me/628113435333?text=Halo%2C+saya+tertarik+dengan+Campaign+Lab">Contact Us Via WA</a>
                </div>
                <!-- End of .main-nav-toggler -->
            </div>
            <!-- End of .navbar-inner -->
        </div>
        <!-- End of .container -->
    </nav>
    <!-- End of .navbar -->
</header>