<footer class="page-footer bg-grey-dark-key">
    <div class="container">
        <!-- End of .footer-mid -->
        <d class="footer-bottom">

            <!-- End of .footer-bottom-links -->
            <p class="axil-copyright-txt">
                © 2021. All rights reserved by Campaign Lab .
            </p>
        </d>
        <!-- End of .footer-bottom -->
    </div>
    <!-- End of .container -->
</footer>