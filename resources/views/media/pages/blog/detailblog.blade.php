@extends('layouts.media')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<div class="main-content">
<div class="breadcrumb-wrapper">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/news">Blog</a></li>
                {{-- <li class="breadcrumb-item"><a href="#">Categories</a></li> --}}
                <li class="breadcrumb-item active" aria-current="page">{{ $blog->title_blog }}</li>
            </ol>
            <!-- End of .breadcrumb -->
        </nav>
    </div>
    <!-- End of .container -->
</div>
<!-- End of .breadcrumb-container -->

<!-- Banner starts -->
<section class="banner banner__single-post banner__standard">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="post-title-wrapper">
                    <div class="btn-group">
                        <a href="#" class="cat-btn bg-color-blue-one">{{ $blog->category['name_category_blog']}}</a>
                    </div>

                    <h2 class="m-t-xs-20 m-b-xs-0 axil-post-title hover-line">{{ $blog->title_blog }}
                    </h2>
                    <div class="post-metas banner-post-metas m-t-xs-20">
                        <ul class="list-inline">
                            <li><a href="#" class="post-author post-author-with-img"><img style="border-radius: 50px;"
                                        src="{{ asset('assets/author/img/'.$blog->author['photo'])}}" alt="author">{{ $blog->author['nama']}}</a></li>
                            <li><a href="#"><i class="feather icon-activity"></i>
                                <span class="rating-result  mr-shortcode rating-result-12194" style="float: right;">
                                    <span class="no-rating-results-text ratingblog">
                                        @if($blog->userAverageRating > 0)
                                        <input id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="{{ $blog->averageRating }}" data-size="sm" disabled="">
                                        @else
                                        No Ratings Yet.
                                        @endif
                                    </span>
                                </span>
                                    </a>
                                </li>
                        </ul>
                    </div>
                    <!-- End of .post-metas -->
                </div>
                <!-- End of .post-title-wrapper -->
            </div>
            <!-- End of .col-lg-6 -->

            <div class="col-lg-6">
                <img src="{{ asset('assets/blog/img/'.$blog->photo_blog)}}" alt="" class="img-fluid" width="600" height="600">
            </div>
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .banner -->

<!-- post-single-wrapper starts -->
<div class="post-single-wrapper p-t-xs-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <main class="site-main">
                    <article class="post-details">
                        <div class="single-blog-wrapper">
                            <div class="post-details__social-share mt-2">
                                <ul class="social-share social-share__with-bg social-share__vertical">
                                    <li><a href="<?php Share::currentPage()->facebook()->getRawLinks();?>"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="<?php Share::currentPage()->twitter()->getRawLinks();?>"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="<?php Share::currentPage()->linkedin()->getRawLinks();?>"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                                <!-- End of .social-share__no-bg -->
                            </div>
                            <!-- End of .social-share -->

                            <p> {!! $blog->content_blog !!}</p>
                            <!-- End of .blockquote -->
                        </div>
                        <!-- End of .single-blog-wrapper -->
                    </article>
                    <!-- End of .post-details -->
                    <!-- End of .post-shares -->

                    <hr class="m-t-xs-50 m-b-xs-60">

                    <div class="about-author m-b-xs-60">
                        <div class="media">
                            <a href="#">
                                    <?php
                                    if(isset($blog->author['photo'])){
                                    ?>
                               <img style="border-radius: 50px; width:105px;" src="{{ asset('assets/author/img/'.$blog->author['photo'])}}" alt="{{$blog->author['nama']}}" class="author-img">
                                                    <?php    
                                                    }else{
                                                ?>
                                                <img style="border-radius: 50px; width:105px;" src="{{ asset('backend/assets/img/theme/boy.png') }}" alt="{{$blog->author['nama']}}" class="author-img">
                                                <?php
                                                    }
                                                ?>
                                                </a>
                            <div class="media-body">
                                <div class="media-body-title">
                                    <h3><a href="#">{{$blog->author['nama']}}</a></h3>
                                </div>
                                <!-- End of .media-body-title -->

                                <div class="media-body-content">
                                    <p>{!!$blog->author['deskripsi_singkat']!!}</p>
                                    <ul class="social-share social-share__with-bg">
                                        @if($author->link_sosmed)
                                        @foreach($author->link_sosmed as $link)
                                           
                                                @if($link['url'] != '' || $link['url'] != null)
                                                <li><a target="_blank" class="sosmed-author" href="{{ $link['url'] }}"><i class="{{ $link['icon'] }}"></i></a></li>
                                                @endif
                                            
                                        @endforeach
                                        @endif
                                    </ul>
                                    <!-- End of .social-share__no-bg -->
                                </div>
                                <!-- End of .media-body-content -->
                            </div>
                        </div>
                    </div>
                    <!-- End of .about-author -->
                    <div class="rating-form mr-shortcode">
                        <form action="{{ url('/news/ratingblog') }}" method="POST">
                                {{ csrf_field() }}
                            <p class="rating-item mr "><label for="rating-item-1-1" style="display:block;"class="description">Apakah artikel ini
                                    membantu kamu?</label> 
                                    <div class="rating">
                                    <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $blog->userAverageRating }}" data-size="xs">
                                    <input type="hidden" name="id" required="" value="{{ $blog->id }}">
                                    <br/>
                                    {{-- @guest
                                    <button type="submit" class="btn-author btn btn-default save-rating">Anda harus login terlebih dahulu</button>
                                    @else --}}
                                    <button type="submit" class="btn-author btn btn-default save-rating">Submit Rating</button>
                                    {{-- @endguest --}}
                                    </div>
                                </form>
                    </div>
                    <?php $url= url('/news/'.$blog->slug); ?>
                    <?php $identifier= '$blog->slug'; ?>
                    <div id="disqus_thread"></div>
                    <script type="text/javascript">
                    var url="<?php echo $url; ?>";
                    var identifier="<?php echo $identifier; ?>";
                        var disqus_config = function () {
                        this.page.url = url;  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = identifier; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };
                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://campaignlab.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    <!-- End of .post-navigation -->
                </main>
                <!-- End of main -->
            </div>
            <!--End of .col-auto  -->

            <div class="col-lg-4">
                <aside class="post-sidebar">
                    <div class="add-block-widget m-b-xs-40">
                        <a href="#"><img src="{{ asset('media/assets/images/sidebar-add.jpg') }}" alt="sidebar add"
                            class="img-fluid"></a>
                    </div>
                    <div class="newsletter-widget weekly-newsletter bg-color-white m-b-xs-40">
                        <div class="newsletter-content">
                            <iframe width="100%" height="640px" src="https://3f02b908.sibforms.com/serve/MUIEAHe8_VNYhSgXymXliLxn2IDe5ghJQetEeqJI3y5_EIO8RCvcSWktZzTyK1a5rlOR-jlpOcOgiDnStf3_UYlIlij1uAOuXwQ6CypLylfH1j16IjiTjq0q25WiKfCEM_RH3wctjDHM43F-T973nqC9BQZ6CDgxvOPu8_ZTBUY7VChfE4fEyR5EdNYD55dPOL6UErPJk2pMnFBt" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>
                            <!-- End of .subscription-form-wrapper -->
                        </div>
                        <!-- End of .newsletter-content -->
                    </div>
                    <!-- End of  .newsletter-widget -->

                    <div class="category-widget m-b-xs-40">
                        <div class="widget-title">
                            <h3>Categories</h3>
                        </div>

                        <div class="category-carousel">
                            <div class="owl-carousel owl-theme">
                                <div class="cat-carousel-inner">
                                    <ul class="category-list-wrapper">
                                        @foreach ($category as $item)
                                        <li class="category-list perfect-square">
                                            <a href="#" class="list-inner"
                                                style="background-image: url({{ asset('assets/categoryblog/img/'.$item->photo)}}">
                                                <div class="post-info-wrapper overlay">
                                                    <div class="counter-inner"><span class="counter">{{ $item->blog->count() }}</span>
                                                    </div>
                                                    <h4 class="cat-title">{{ $item->name_category_blog}}</h4>
                                                </div>
                                                <!-- End of .counter-wrapper -->
                                            </a>
                                        </li>
                                        @endforeach
                                        <!-- End of .category-list -->
                                    </ul>
                                    <!-- End of .category-list-wrapper -->
                                </div>
                                <!-- End of .cat-carousel-inner -->
                            </div>
                            <!-- End of  .owl-carousel -->
                        </div>
                        <!-- End of .category-carousel -->
                    </div>
                    <!-- End of .category-widget -->

                    
                    <!-- End of .sidebar-social-share -->

                    <div class="post-widget sidebar-post-widget m-b-xs-40">
                        <div class="widget-title">
                            <h3>Recent Post</h3>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="recent-post">
                                <div class="axil-content">
                                    @foreach($lastblog as $item)
                                    <div class="media post-block post-block__small">
                                        <a href="{{ url('/news/'.$item->slug) }}" class="align-self-center"><img
                                                class=" m-r-xs-30" src="{{ asset('assets/blog/img/'.$item->photo_blog)}}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <div class="post-cat-group">
                                                <a href="{{ url('/news/'.$item->slug) }}"
                                                    class="post-cat color-blue-three">{{ $item->category['name_category_blog']}}</a>
                                            </div>
                                            <h4 class="axil-post-title hover-line hover-line"><a
                                                    href="{{ url('/news/'.$item->category['slug']) }}">{{ $item->name_category_blog}}</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">{{$item->author['nama']}}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <!-- End of .post-block -->
                                </div>
                                <!-- End of .content -->
                            </div>
                            <!-- End of .tab-pane -->
                            <!-- End of .tab-pane -->
                        </div>
                        <!-- End of .tab-content -->
                    </div>
                    <!-- End of .sidebar-post-widget -->

                    <div class="tag-widget m-b-xs-30">
                        <div class="widget-title">
                            <h3>Tags</h3>
                        </div>
                        <div class="axil-content">
                            <ul class="tag-list-wrapper">
                                @foreach($blog->tags as $tag)
                                <li><a href="{{ url('/news/tag/'.$tag->name) }}" rel="tag">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                            <!-- End of .tab-list-wrapper -->
                        </div>
                        <!-- End of .content -->
                    </div>
                    <!-- End of .tag-widget -->
                    <!-- End of .instagram-widget -->

                    <div class="add-block-widget m-b-xs-40">
                        <a href="#"><img src="{{ asset('media/assets/images/sidebar-add.jpg') }}" alt="sidebar add"
                                class="img-fluid"></a>
                    </div>
                    <!-- End of .add-block-widget -->
                </aside>
                <!-- End of .post-sidebar -->
            </div>
            <!-- End of .col-lg-4 -->
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</div>
<!-- End of .post-single-wrapper -->

<section class="related-post p-b-xs-30">
    <div class="container">
        <div class="section-title m-b-xs-40 mt-5">
            <h2 class="axil-title">Article Terkait</h2>
        </div>
        <!-- End of .section-title -->

        <div class="grid-wrapper">
            <div class="row">
                @foreach ($blogTerkait as $item)
                <div class="col-lg-3 col-md-4">
                    <div class="content-block m-b-xs-30">
                        <a href="{{ url('/news/'.$item->slug) }}">
                            <img src="{{ asset('assets/blog/img/'.$item->photo_blog)}}" alt="abstruct image"
                                class="img-fluid">
                            <div class="grad-overlay"></div>
                        </a>
                        <div class="media-caption">
                            <div class="caption-content">
                                <h3 class="axil-post-title hover-line hover-line"><a
                                        href="{{ url('/news/'.$item->slug) }}">{!! Str::limit($item->title_blog, 50) !!}</a></h3>
                                <div class="caption-meta">
                                    By <a href="#">{{ $blog->author['nama']}}</a>
                                </div>
                            </div>
                            <!-- End of .content-inner -->
                        </div>
                    </div>
                    <!-- End of .content-block -->
                </div>
                @endforeach
                <!-- End of .col-lg-3 -->
            </div>
            <!-- End of .row -->
        </div>
        <!-- End of .grid-wrapper -->
    </div>
    <!-- End of .container -->
</section>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
	<script type="text/javascript">
		$("#input-id").rating();
	</script>
<!-- End of .related-post -->
@endsection