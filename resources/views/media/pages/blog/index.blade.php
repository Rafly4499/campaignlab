@extends('layouts.media')

@section('content')
<section class="banner banner__home-with-slider banner__home-with-slider-two grad-bg">
    <div class="axil-shape-circle"></div>
    <div class="axil-shape-circle__two"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
                <div class="banner-slider-container banner-slider-container-two">
                    <div id="sync2" class="slick-slider slick-slider-for" data-slick-nav="true">
                        @foreach ($newblog as $item)
                            
                        
                        <div class="item">
                            <div class="post-metas home-banner-post-metas m-b-xs-20">
                                <ul class="list-inline">
                                    <li class="m-r-xs-20">
                                        <a href="#" class="d-flex align-items-center">
                                            <img style="border-radius: 50px;" src="{{ asset('assets/author/img/'.$item->author['photo'])}}" alt="" class="m-r-xs-20">
                                            <span>{{ $item->author['nama']}}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- End of .post-metas -->
                            <h1 class="page-title m-b-xs-40 hover-line">
                                <a href="{{ url('/news/'.$item->slug) }}">
                                    {{ Str::limit($item->title_blog, 50) }}
                                </a>
                            </h1>
                            <div class="btn-group">
                                <a href="{{ url('/news/'.$item->slug) }}" class="btn btn-primary m-r-xs-30">READ MORE</a>
                                <a href="{{ url('/news') }}" class="btn-link">ALL CURRENT NEWS</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- End of .owl-carousel -->
                </div>
                <!-- End of .banner-slider-container -->
            </div>
            <!-- End of .col-lg-6 -->
        </div>
        <!-- End of .row -->
        <div class="banner-slider-container-synced banner-slider-container-synced__two">
            <div id="sync1" class="slick-slider slick-slider-nav" data-slick-loop="true" data-slick-dots="false"
                data-slick-items="2" data-slick-center="true" data-slick-autowidth="true">
                @foreach ($newblog as $item)
                <div class="item">
                    <img src="{{ asset('assets/blog/img/'.$item->photo_blog)}}"
                        alt="{{ Str::limit($item->title_blog, 30) }}">
                </div>
                @endforeach
            </div>
            <!-- End of .owl-carousel -->
        </div>
        <!-- End of .banner-slider-container-synced -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .banner -->
<div class="random-posts section-gap-bottom bg-grey-light-three">
    <div class="axil-banner-cat-counter">
        <div class="container">
            <div class="axil-content">
                <ul class="category-list-wrapper">
                    @foreach ($category as $item)
                    <li class="category-list perfect-square">
                        <a href="#" class="list-inner"
                            style="background-image: url({{ asset('assets/categoryblog/img/'.$item->photo)}}">
                            <div class="post-info-wrapper overlay">
                                <div class="counter-inner"><span class="counter">{{ $item->blog->count() }}</span>
                                </div>
                                <h4 class="cat-title">{{ $item->name_category_blog}}</h4>
                            </div>
                            <!-- End of .counter-wrapper -->
                        </a>
                    </li>
                    @endforeach
                    <!-- End of .category-list -->
                    <!-- End of .category-list -->
                </ul>
                <!-- End of .category-list-wrapper -->
            </div>
            <!-- End of .axil-content -->
        </div>
        <!-- End of .container -->
    </div>
    <!-- End of .axil-banner-cat-counter -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <main class="axil-content">
                    @foreach ($blog as $item)
                    <div class="media post-block post-block__mid m-b-xs-30">
                        <a href="{{ url('/news/'.$item->slug) }}" class="align-self-center"><img class=" m-r-xs-30"
                                src="{{ asset('assets/blog/img/'.$item->photo_blog)}}" alt=""></a>
                        <div class="media-body">
                            <div class="post-cat-group m-b-xs-10">
                                <a href="{{ url('/news/category/'.$item->category['slug']) }}" class="post-cat cat-btn bg-color-blue-one">{{ $item->category['name_category_blog'] }}</a>
                            </div>
                            <h3 class="axil-post-title hover-line"><a href="{{ url('/news/'.$item->slug) }}">{{ Str::limit($item->title_blog, 50) }}</a></h3>
                            <p class="mid">{!! Str::limit($item->content_blog, 100) !!}</p>
                            <div class="post-metas">
                                <ul class="list-inline">
                                    <li>By <a href="#">{{ $item->author['nama']}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- End of .post-block -->
                    <!-- End of .post-block -->
                </main>
                <!-- End of .axil-content -->
            </div>
            <!-- End of .col-lg-8 -->
            <div class="col-lg-4">
                <aside class="post-sidebar">
                    <!-- End of .sidebar-social-share -->
                    <div class="newsletter-widget weekly-newsletter bg-color-white m-b-xs-40">
                        <div class="newsletter-content">
                            <iframe width="100%" height="640px" src="https://3f02b908.sibforms.com/serve/MUIEAHe8_VNYhSgXymXliLxn2IDe5ghJQetEeqJI3y5_EIO8RCvcSWktZzTyK1a5rlOR-jlpOcOgiDnStf3_UYlIlij1uAOuXwQ6CypLylfH1j16IjiTjq0q25WiKfCEM_RH3wctjDHM43F-T973nqC9BQZ6CDgxvOPu8_ZTBUY7VChfE4fEyR5EdNYD55dPOL6UErPJk2pMnFBt" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>
                            <!-- End of .subscription-form-wrapper -->
                        </div>
                        <!-- End of .newsletter-content -->
                    </div>
                    <div class="category-widget m-b-xs-40">
                        <div class="widget-title">
                            <h3>Categories</h3>
                            {{-- <div class="owl-nav">
                                <button class="custom-owl-prev"><i
                                        class="feather icon-chevron-left"></i></button>
                                <button class="custom-owl-next"><i
                                        class="feather icon-chevron-right"></i></button>
                            </div> --}}
                        </div>
                        <div class="category-carousel">
                            <div class="owl-carousel owl-theme">
                                <div class="cat-carousel-inner">
                                    <ul class="category-list-wrapper">
                                        @foreach ($allcategory as $item)
                                        <li class="category-list perfect-square">
                                            <a href="{{ url('/news/category/'.$item->slug) }}" class="list-inner"
                                                style="background-image: url({{ asset('assets/categoryblog/img/'.$item->photo)}}">
                                                <div class="post-info-wrapper overlay">
                                                    <div class="counter-inner"><span class="counter">{{ $item->blog->count() }}</span>
                                                    </div>
                                                    <h4 class="cat-title">{{ $item->name_category_blog}}</h4>
                                                </div>
                                                <!-- End of .counter-wrapper -->
                                            </a>
                                        </li>
                                        @endforeach
                                        <!-- End of .category-list -->
                                    </ul>
                                    <!-- End of .category-list-wrapper -->
                                </div>
                                <!-- End of .cat-carousel-inner -->
                            </div>
                            <!-- End of  .owl-carousel -->
                        </div>
                        <!-- End of .category-carousel -->
                    </div>
                    <!-- End of .category-widget -->
                    {{-- <div class="post-widget sidebar-post-widget m-b-xs-40">
                        <ul class="nav nav-pills row no-gutters">
                            <li class="nav-item col">
                                <a class="nav-link active" data-toggle="pill" href="#recent-post">Recent</a>
                            </li>
                            <li class="nav-item col">
                                <a class="nav-link" data-toggle="pill" href="#popular-post">Popular</a>
                            </li>
                            <li class="nav-item col">
                                <a class="nav-link" data-toggle="pill" href="#comments">Comments</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="recent-post">
                                <div class="axil-content">
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-1.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <div class="post-cat-group">
                                                <a href="post-format-standard.html"
                                                    class="post-cat color-blue-three">BEAUTY,</a>
                                            </div>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Stocking Your Restaurant
                                                    Kitchen Finding Reliable
                                                    Sellers</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Amachea Jajah</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-2.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-green-three">TRAVEL</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Trip
                                                    To Iqaluit In Nunavut A
                                                    Canadian Arctic
                                                    City</a>
                                            </h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Xu Jianhong</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-3.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-red-two">SPORTS</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Thousands Now Adware
                                                    Removal Who Never Thought </a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Ahmad Nazeri</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-4.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-blue-one">FASHION</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">To
                                                    Keep Makeup Looking Fresh
                                                    Take A Powder</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Sergio Pliego</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                </div>
                                <!-- End of .content -->
                            </div>
                            <!-- End of .tab-pane -->
                            <div class="tab-pane fade" id="popular-post">
                                <div class="axil-content">
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-4.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-blue-one">FASHION</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">To
                                                    Keep Makeup Looking Fresh
                                                    Take A Powder</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Sergio Pliego</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-3.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="#post-format-standard.html"
                                                class="post-cat color-blue-three">BEAUTY</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Stocking Your Restaurant
                                                    Kitchen Finding Reliable
                                                    Sellers</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Amachea Jajah</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-2.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-green-three">TRAVEL</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Trip
                                                    To Iqaluit In Nunavut A
                                                    Canadian Arctic
                                                    City</a>
                                            </h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Xu Jianhong</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-1.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-red-two">SPORTS</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">RCB
                                                    vs RR, IPL 2019:
                                                    Bangalore, Rajasthan desperate
                                                    for
                                                    win</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Ahmad Nazeri</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                </div>
                                <!-- End of .content -->
                            </div>
                            <!-- End of .tab-pane -->
                            <div class="tab-pane fade" id="comments">
                                <div class="axil-content">
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-3.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-red-two">SPORTS</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">RCB
                                                    vs RR, IPL 2019:
                                                    Bangalore, Rajasthan desperate
                                                    for
                                                    win</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Ahmad Nazeri</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-4.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-blue-three">BEAUTY</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Stocking Your Restaurant
                                                    Kitchen Finding Reliable
                                                    Sellers</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Amachea Jajah</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-2.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-green-three">TRAVEL</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">Trip
                                                    To Iqaluit In Nunavut A
                                                    Canadian Arctic
                                                    City</a>
                                            </h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="post-format-standard.html">Xu Jianhong</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                    <div class="media post-block post-block__small">
                                        <a href="post-format-standard.html" class="align-self-center"><img
                                                class=" m-r-xs-30"
                                                src="{{ asset('media/assets/images/illustration/small-media/small-media-1.jpg') }}"
                                                alt="media image"></a>
                                        <div class="media-body">
                                            <a href="post-format-standard.html"
                                                class="post-cat color-blue-one">FASHION</a>
                                            <h4 class="axil-post-title hover-line"><a
                                                    href="post-format-standard.html">To
                                                    Keep Makeup Looking Fresh
                                                    Take A Powder</a></h4>
                                            <div class="post-metas">
                                                <ul class="list-inline">
                                                    <li>By <a href="#">Sergio Pliego</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of .post-block -->
                                </div>
                                <!-- End of .content -->
                            </div>
                            <!-- End of .tab-pane -->
                        </div>
                        <!-- End of .tab-content -->
                    </div> --}}
                    <!-- End of .sidebar-post-widget -->
                
                    <!-- End of .instagram-widget -->
                </aside>
                <!-- End of .post-sidebar -->
            </div>
            <!-- End of .col-lg-4 -->
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</div>
@endsection