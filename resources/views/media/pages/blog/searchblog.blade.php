@extends('layouts.media')

@section('content')
<section class="banner banner__home-with-slider banner__home-with-slider-two grad-bg">
    <div class="axil-shape-circle"></div>
    <div class="axil-shape-circle__two"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
                <div class="banner-slider-container banner-slider-container-two">
                    <div id="sync2" class="slick-slider slick-slider-for" data-slick-nav="true">
                        <div class="item">
                            @if($blog->count() == 0)
                            <h1 class="page-title m-b-xs-40 hover-line">
                                Hasil pencarian pada "{{ $search }}"
                            </h1>
                            <h4>Pencarian Anda Tidak Ditemukan</h4>
                            @else
                            <h1 class="page-title m-b-xs-40 hover-line">
                                Hasil pencarian pada "{{ $search }}"
                            </h1>
                            @endif
                        </div>
                    </div>
                    <!-- End of .owl-carousel -->
                </div>
                <!-- End of .banner-slider-container -->
            </div>
            <!-- End of .col-lg-6 -->
        </div>

        <!-- End of .banner-slider-container-synced -->
    </div>
    <!-- End of .container -->
</section>
<!-- End of .banner -->
<div class="random-posts section-gap-bottom bg-grey-light-three">
    <div class="axil-banner-cat-counter">
        <div class="container">
            <div class="axil-content">
                <ul class="category-list-wrapper">
                    @foreach ($topcategory as $item)
                    <li class="category-list perfect-square">
                        <a href="#" class="list-inner"
                            style="background-image: url({{ asset('assets/categoryblog/img/'.$item->photo)}}">
                            <div class="post-info-wrapper overlay">
                                <div class="counter-inner"><span class="counter">{{ $item->blog->count() }}</span>
                                </div>
                                <h4 class="cat-title">{{ $item->name_category_blog}}</h4>
                            </div>
                            <!-- End of .counter-wrapper -->
                        </a>
                    </li>
                    @endforeach
                    <!-- End of .category-list -->
                    <!-- End of .category-list -->
                </ul>
                <!-- End of .category-list-wrapper -->
            </div>
            <!-- End of .axil-content -->
        </div>
        <!-- End of .container -->
    </div>
    <!-- End of .axil-banner-cat-counter -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <main class="axil-content">
                    @foreach ($blog as $item)
                    <div class="media post-block post-block__mid m-b-xs-30">
                        <a href="{{ url('/news/'.$item->slug) }}" class="align-self-center"><img class=" m-r-xs-30"
                                src="{{ asset('assets/blog/img/'.$item->photo_blog)}}" alt=""></a>
                        <div class="media-body">
                            <div class="post-cat-group m-b-xs-10">
                                <a href="{{ url('/news/category/'.$item->category['slug']) }}" class="post-cat cat-btn bg-color-blue-one">{{ $item->category['name_category_blog'] }}</a>
                            </div>
                            <h3 class="axil-post-title hover-line"><a href="{{ url('/news/'.$item->slug) }}">{{ Str::limit($item->title_blog, 50) }}</a></h3>
                            <p class="mid">{!! Str::limit($item->content_blog, 100) !!}</p>
                            <div class="post-metas">
                                <ul class="list-inline">
                                    <li>By <a href="#">{{ $item->author['nama']}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- End of .post-block -->
                    <!-- End of .post-block -->
                </main>
                <!-- End of .axil-content -->
            </div>
            <!-- End of .col-lg-8 -->
            <div class="col-lg-4">
                <aside class="post-sidebar">
                    <!-- End of .sidebar-social-share -->
                    <div class="newsletter-widget weekly-newsletter bg-color-white m-b-xs-40">
                        <div class="newsletter-content">
                            <iframe width="100%" height="640px" src="https://3f02b908.sibforms.com/serve/MUIEAHe8_VNYhSgXymXliLxn2IDe5ghJQetEeqJI3y5_EIO8RCvcSWktZzTyK1a5rlOR-jlpOcOgiDnStf3_UYlIlij1uAOuXwQ6CypLylfH1j16IjiTjq0q25WiKfCEM_RH3wctjDHM43F-T973nqC9BQZ6CDgxvOPu8_ZTBUY7VChfE4fEyR5EdNYD55dPOL6UErPJk2pMnFBt" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>
                            <!-- End of .subscription-form-wrapper -->
                        </div>
                        <!-- End of .newsletter-content -->
                    </div>
                    <div class="category-widget m-b-xs-40">
                        <div class="widget-title">
                            <h3>Categories</h3>
                            {{-- <div class="owl-nav">
                                <button class="custom-owl-prev"><i
                                        class="feather icon-chevron-left"></i></button>
                                <button class="custom-owl-next"><i
                                        class="feather icon-chevron-right"></i></button>
                            </div> --}}
                        </div>
                        <div class="category-carousel">
                            <div class="owl-carousel owl-theme">
                                <div class="cat-carousel-inner">
                                    <ul class="category-list-wrapper">
                                        @foreach ($category as $item)
                                        <li class="category-list perfect-square">
                                            <a href="{{ url('/news/category/'.$item->slug) }}" class="list-inner"
                                                style="background-image: url({{ asset('assets/categoryblog/img/'.$item->photo)}}">
                                                <div class="post-info-wrapper overlay">
                                                    <div class="counter-inner"><span class="counter">{{ $item->blog->count() }}</span>
                                                    </div>
                                                    <h4 class="cat-title">{{ $item->name_category_blog}}</h4>
                                                </div>
                                                <!-- End of .counter-wrapper -->
                                            </a>
                                        </li>
                                        @endforeach
                                        <!-- End of .category-list -->
                                    </ul>
                                    <!-- End of .category-list-wrapper -->
                                </div>
                                <!-- End of .cat-carousel-inner -->
                            </div>
                            <!-- End of  .owl-carousel -->
                        </div>
                        <!-- End of .category-carousel -->
                    </div>
                </aside>
                <!-- End of .post-sidebar -->
            </div>
            <!-- End of .col-lg-4 -->
        </div>
        <!-- End of .row -->
    </div>
    <!-- End of .container -->
</div>
@endsection