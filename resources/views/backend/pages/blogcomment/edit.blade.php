@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Comment Publication</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Comment Publication</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Edit Status</h3>
          </div>

          <div class="card-body">

                    <form action="{{ url('/admin/commentarticle/'.$comment->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="col-md-12">
                          <div class="form-group">
                              <label for="fullname">Status</label>
                              <select class="form-control select2" value="{{ $comment->status }}" name="status" id="" required>
                      
                          <option value="1" {{ $comment->status == 1 ? 'selected' : '' }}>Diizinkan</option>
                          <option value="0" {{ $comment->status == 0 ? 'selected' : '' }}>Pending</option>
                      
                      </select>
                          </div>
                      </div>
                        <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                        <a href="{{ url('/admin/commentarticle') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
