@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Commentar article</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">article</a></li>
                <li class="breadcrumb-item active" aria-current="page">Commentar article</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Commentar article</h3>
          </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                          <thead class="thead-light">
                            <tr>
                                    <th scope="col" class="sort" data-sort="name">No</th>
                                                <th scope="col" class="sort" data-sort="name">User</th>
                                                <th scope="col" class="sort" data-sort="name">Review</th>
                                                <th scope="col" class="sort" data-sort="name">Status</th>
                                                <th scope="col" class="sort" data-sort="name">Action</th>
                                </tr>
                            </thead>
             
                            <tbody class="list">
                                <?php $no = 0;?> @foreach($comment as $item)
                                <?php $no++;?>
                                <tr>
                                  <td>{{ $no }}</td>
                                  <td>{{ $item->user['name'] }}</td>
                                  <td>{{ $item->content }}</td>
                                  <td>
                                      @if($item->status == 1)
                                          <button class="btn btn-rounded btn-sm btn-success">Diizinkan</button>
                                      @elseif($item->status == 0)
                                      <button class="btn btn-rounded btn-sm btn-warning">Pending</button>
                                      @endif
                                  </td>
                                    <td>
                                        <form action="{{ url('/admin/commentarticle/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/admin/commentarticle/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
