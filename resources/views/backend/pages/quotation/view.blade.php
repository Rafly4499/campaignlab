@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Quotation</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Quotation Message</a></li>
                <li class="breadcrumb-item active" aria-current="page">View</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">View Data</h3>
          </div>

          <div class="card-body">
                                <a href="{{ url('/admin/quotation') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                                <div class="col-md-12">
                                    <div class="row">
                                       
                                        <label class="col-md-4">Tanggal</label></label>
                                        
                                        <div class="col-md-8"><p>:{{  date('d M Y', strtotime($quotation->created_at)) }}</p> </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama PIC</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$quotation->name_pic}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">No Handphone</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$quotation->no_hp}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Email</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$quotation->email}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Company</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$quotation->company}}</p> </div>
                                                    </div>
                                                </div>     
                                                <div class="col-md-12">
                                                  <div class="row">
                                                     
                                                      <label class="col-md-4">Project</label>
                                                      
                                                      <div class="col-md-8"><p>:{{$quotation->project}}</p> </div>
                                                  </div>
                                              </div>    
                                              <div class="col-md-12">
                                                <div class="row">
                                                   
                                                    <label class="col-md-4">Description</label>
                                                    
                                                    <div class="col-md-8"><p>:{{$quotation->description}}</p> </div>
                                                </div>
                                            </div>     
                                            <div class="col-md-12">
                                              <div class="row">
                                                 
                                                  <label class="col-md-4">References</label>
                                                  
                                                  <div class="col-md-8"><p>:{{$quotation->references}}</p> </div>
                                              </div>
                                          </div> 
                                                                                                                                                                 
                            </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
            </div>
            @endsection