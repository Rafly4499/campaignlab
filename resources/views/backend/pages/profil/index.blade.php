@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Profile</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Profile</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h2 class="section-title">Hi, {{ Auth::guard('admin')->user()->name }}!</h2>
            <h3 class="mb-0">Change information about yourself on this page.</h3>
          </div>
          <div class="card-body">
    <div class="row mt-sm-4">
      <div class="col-md-2">
        <div class="card profile-widget">
          <div class="profile-widget-description">
            <img src="{{ asset('backend/assets/img/theme/boy.png') }}" class="img-responsive img-fluid" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-10">
        <div class="card">
          <form method="post" class="needs-validation" action="{{ route('update.profile') }}">
            @csrf
            <div class="card-header">
              <h4>Edit Profile</h4>
            </div>
            <div class="card-body">
                <div class="row">                               
                  <div class="form-group col-12">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="name" value="{{ Auth::guard('admin')->user()->name }}" required="">
                    <div class="invalid-feedback">
                      Please fill in the first name
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12 col-12">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ Auth::guard('admin')->user()->email }}" required="">
                    <div class="invalid-feedback">
                      Please fill in the email
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      @component('backend.components.form-input', [
                          'label' => 'Password',
                          'type' => 'password',
                          'name' => 'password',
                          'value' => old('password'),
                          'error' => $errors->first('password'),
                      ])
                      @endcomponent
                  </div>
                  <div class="col-md-6">
                      @component('backend.components.form-input', [
                          'label' => 'Konfirmasi Password',
                          'type' => 'password',
                          'name' => 'password_confirmation',
                          'value' => old('password_confirmation'),
                          'error' => $errors->first('password_confirmation'),
                      ])
                      @endcomponent
                  </div>
                </div>
            </div>
            <div class="card-footer text-right">
              <button class="btn btn-primary">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection