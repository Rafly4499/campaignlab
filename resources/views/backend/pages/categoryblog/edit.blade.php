@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Category Blog</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Category Blog</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Edit Data</h3>
          </div>

          <div class="card-body">
                    <form action="{{ url('/admin/categoryblog/'.$category->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Kategori Blog</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $category->name_category_blog }}" name="name_category_blog">
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="" class="col-sm-2 control-label">Image</label>
                          <img style="border-radius: 5px; width:120px" src="{{ url('assets/categoryblog/img/'.$category->photo) }}" alt="">
                              <div class="custom-file mt-4">
                                  <input type="file" name="image" class="custom-file-input" id="customFileLang preview" lang="en" data-default-file="{{ $category->photo }}" value="{{ $category->photo }}">
                                  <label class="custom-file-label" for="customFileLang">Select file</label>
                              </div>
                              
                             
                      </div>
                        <button type="submit" name="submit" class="btn btn-primary">SAVE</button>
                        <a href="{{ url('/admin/categoryblog') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
