@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Blog</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Blog</a></li>
                <li class="breadcrumb-item active" aria-current="page">List Blog</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">List Blog</h3>
          </div>
          <div class="card-header">
                    <a href="{{ url('/admin/blog/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a>
                </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                          <thead class="thead-light">
                            <tr>
                              <th scope="col" class="sort" data-sort="name">No</th>
                              <th scope="col" class="sort" data-sort="budget">Title Blog</th>
                              <th scope="col" class="sort" data-sort="budget">Category Blog</th>
                              <th scope="col" class="sort" data-sort="budget">Tag Blog</th>
                              <th scope="col" class="sort" data-sort="budget">Author</th>
                              <th scope="col" class="sort" data-sort="budget">Published On</th>
                              <th scope="col" class="sort" data-sort="budget">Status</th>
                              <th scope="col" class="sort" data-sort="status">Action</th>
                            </tr>
                          </thead>
                          <tbody class="list">
                                <?php $no = 0;?> @foreach($blog as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td style="width:100px !important;">{{ Str::limit($item->title_blog, 30) }}</td>
                                    <td>{{ $item->categoryblog['name_category_blog'] }}</td>
                                    <td>
                                        
                                    @foreach($item->tags->take(1) as $tag)
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-info">{{ $tag->name }}</button>
                                    @endforeach
                                    <?php 
                                      $count = count($item->tags);
                                      $counttag = $count - 1;
                                    ?>
                                    @if($counttag != 0)
                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info btn-sm">+ {{$counttag}}</button>
                                  @endif  
                                  </td>
                                    <td>{{ $item->author['nama'] }}</td>
                                    <td>{{ $item->published_at }}</td>
                                    <td>
                                      @if($item->status_draft == "true")
                                      <button type="button" class="btn waves-effect waves-light btn-rounded btn-warning">Draft</button>
                                      
                                      @elseif($item->status_draft == "false")
                                      <button type="button" class="btn waves-effect waves-light btn-rounded btn-success">Published</button>
                                      
                                      @endif
                                    </td>
                                    <td>
                                        <form action="{{ url('/admin/blog/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/admin/blog/'.$item->id.'/edit') }}" class="btn btn-success btn-rounded"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="{{ url('/admin/blog/'.$item->id.'/view') }}" class="btn btn-info btn-rounded"><i class="fas fa-eye"></i></a>
                                            <button type="submit" class="btn btn-danger btn-rounded" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>

                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
