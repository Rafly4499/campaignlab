@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Blog</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Blog</a></li>
                <li class="breadcrumb-item active" aria-current="page">View</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">View Data</h3>
          </div>

          <div class="card-body">
                                <a href="{{ url('/admin/blog') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Judul Blog</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->title_blog}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Isi Blog    :</label>
                                                        
                                                        <div class="col-md-12"><p>{!! ($blog->content_blog) !!}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Kategori Blog</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->categoryblog['name_category_blog']}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Link Url(Slug Blog)</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->slug}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Tag</label>
                                                        
                                                        <div class="col-md-8"><p>:
                                                        @foreach($blog->tags as $tag)
                                                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-outline-info">{{ $tag->name }}</button>
                                                        @endforeach</p> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Meta Title</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->meta_title}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <label class="col-md-4">Meta Deskripsi    :</label>
                                                        
                                                        <div class="col-md-12"><p>{!! ($blog->meta_description) !!}</p> </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Published On</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$blog->published_at}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Status Draft</label>
                                                        
                                                        <div class="col-md-8"><p>:
                                                            <?php
                                                            if($blog->status_draft == true){
                                                            
                                                            ?>
                                                            Tidak Aktif
                                                            <?php
                                                            }else{
                                                            ?>
                                                            Aktif
                                                            <?php
                                                            }
                                                            ?></p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Postingan</label>
                                                        <div class="col-md-8"><p>:{{$blog->author['nama']}}</p> </div>
                                                    </div>
                                                </div>
                                                
                                    
                                   <a href="{{ url('/admin/blog/'.$blog->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a> 
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Gambar Blog</label>
                                    </div>
                                <div class="col-md-12">
                                <?php
                                    if($blog->photo_blog){
                                ?>
                                <img style="border-radius: 5px; width:100%" src="{{ url('assets/blog/img/'.$blog->photo_blog) }}" alt="">
                                <?php
                                    }else{
                                ?>
                                <h6>Belum Ada Data Gambar</h6>
                                <?php
                                    }
                                ?>
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            @endsection