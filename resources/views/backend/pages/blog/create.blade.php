@extends('layouts.backend') 
@section('content')
<style type="text/css">
    .label-info{
        background-color: #3d93e4;

    }
    .label {
        display: inline-block;
        padding: .5em 1em;
        font-size: 75%;
        line-height: 1;
        font-weight: 700;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,
        border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .bootstrap-tagsinput [data-role='remove']{
        position: relative !important;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Blog</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Blog</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Create Data</h3>
          </div>

          <div class="card-body">
                    <a href="{{ url('/admin/blog') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" id="formblog" action="{{ url('/admin/blog/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Title Blog</label>
                            
                                <input type="text" class="form-control" name="title_blog" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Content Blog</label>
                            
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="content_blog" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Category Blog</label>
                                <select class="form-control select2" name="id_category_blog" id="" required>
                                    @foreach($category as $data)
                                        <option value="{{ $data->id }}">{{ $data->name_category_blog }}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div>                 
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Tags : </label>
                            
                            <input class="form-control" type="text" name="tags" id="tags" data-role="tagsinput" required>
                            
                            <!-- <input class="form-control" data-role="tagsinput" type="text" name="tags" > -->
                            <h6>Ketik Tag yang anda inginkan, kemudian klik enter</h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Meta Title</label>
                            
                                <input type="text" class="form-control metatitle" name="meta_title" required>
                                <span id="infometatitle" class="badge badge-warning form-text float-left">Meta Title melebihi 60 karakter</span>
                                <br>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Meta Description</label>
                            
                                <textarea class="form-control metadescription" rows="3" name="meta_description" required></textarea>
                                <span id="infometadesc" class="badge badge-warning form-text float-left">Meta Description melebihi 160 karakter</span>
                                <br>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">Setting Url(Slug Blog)</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio1" name="seturl" class="custom-control-input" value="automaticurl" checked="">
                                <label class="custom-control-label" for="customRadio1">Automatic</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio2" name="seturl" class="custom-control-input" value="settingurl">
                                <label class="custom-control-label" for="customRadio2">Custom</label>
                            </div>
                                <input type="text" class="form-control urlblog" id='urlblog' name="slug">
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Author</label>
                                <select class="form-control select2" name="author_id" id="" required>
                                    @foreach($author as $data)
                                        <option value="{{ $data->id }}">{{ $data->nama }}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div> 
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Published On</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio3" name="setpublish" value="automatic" class="custom-control-input" checked="">
                                    <label class="custom-control-label" for="customRadio3">Automatic</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio4" name="setpublish" value="settingpublish" class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio4">Set date and time</label>
                                </div>
                                <div class='input-group date'>
                                    <input type="datetime-local" name="published_at" class="form-control datepicker">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Setting Draft</label><br>
                                <label class="custom-toggle">
                                    <input type="checkbox" name="status_draft">
                                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                                </label>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="id_admin" value="{{ Auth::guard('admin')->user()->id }}">
                        <div class="form-group">
                            <label for="" class="control-label">Gambar Blog</label>
                            <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <label for="exampleInputImage">Image:</label>
                                <h6>Gambar tidak boleh lebih dari ukuran 1000×1000 piksel</h6>
                                <input type="file" name="image" id="preview" class="image" required>
                                <input type="hidden" name="x1" value="" />
                                <input type="hidden" name="y1" value="" />
                                <input type="hidden" name="x2" value="" />
                                <input type="hidden" name="y2" value="" />
                                <input type="hidden" name="w" value="" />
                                <input type="hidden" name="h" value="" />
                            </div>
                        </div>
                        </div>
                        <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');
                        </script>
                        <div class="col-md-12">
                            <p><img style="max-width:100% !important;" id="previewimage"/></p>
                            <h6>Select Gambar jika ingin memotong gambar</h6>
                            @if(session('path'))
                                <img src="{{ session('path') }}" />
                            @endif
                        </div>
                        <button type="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form> 
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js"></script>
<script>
    $(document).ready(function () {
        $("#infometatitle").css("display", "none");
        $("#infometadesc").css("display", "none");
        $(".datepicker").css("display", "none");
        $(".urlblog").css("display", "none");
        $('.metatitle').bind('input propertychange', function() {
            if (this.value.length > 60) {
                $("#infometatitle").css("display", "block");
            }else{
                $("#infometatitle").css("display", "none");
            }
        });
        $('.metadescription').bind('input propertychange', function() {
            if (this.value.length > 160) {
                $("#infometadesc").css("display", "block");
            }else{
                $("#infometadesc").css("display", "none");
            }
        });
        $("#customRadio4").click(function () {
            $("#customRadio3").prop("checked");
            $(".datepicker").css("display", "inline-block");
        });

        $("#customRadio3").click(function () {
            $("#customRadio4").prop("checked");
            $(".datepicker").css("display", "none");
        });

        $("#customRadio2").click(function () {
            $("#customRadio1").prop("checked");
            $(".urlblog").css("display", "inline-block");
        });

        $("#customRadio1").click(function () {
            $("#customRadio2").prop("checked");
            $(".urlblog").css("display", "none");
        });
    });
  </script>
  <script>
    window.URL = window.URL || window.webkitURL;
    $("#formblog").submit( function( e ) {
    var form = this;
    e.preventDefault(); //Stop the submit for now
                                //Replace with your selector to find the file input in your form
    var fileInput = $(this).find("#preview")[0],
        file = fileInput.files && fileInput.files[0];

    if( file ) {
        var img = new Image();

        img.src = window.URL.createObjectURL( file );

        img.onload = function() {
            var width = img.naturalWidth,
                height = img.naturalHeight;

            window.URL.revokeObjectURL( img.src );

            if( width > 1000 || height > 1000 ) {
                alert('Ukuran Gambar terlalu besar');
            }
            else {
                form.submit();
                
            }
        };
    }
    else { //No file was input or browser doesn't support client side reading
        form.submit();
    }

});
    jQuery(function($) {
        var image = $("#previewimage");
        var originalHeight = image.naturalHeight;
        var originalWidth = image.naturalWidth;

        $("body").on("change", ".image", function(){

            var imageReader = new FileReader();
            imageReader.readAsDataURL(document.querySelector(".image").files[0]);

            imageReader.onload = function (oFREvent) {
                image.attr('src', oFREvent.target.result).fadeIn();
            };
        });

        console.log('IMG width: ' + originalWidth + ', height: ' + originalHeight);
        $('#previewimage').imgAreaSelect({
            aspectRatio: '16:9',
            handles: true,
            fadeSpeed: 200,
            imageHeight: originalHeight, 
            imageWidth: originalWidth,
            onSelectChange: getCoordinates
            });
        function getCoordinates(img, selection) {

            if (!selection.width || !selection.height){
            return;
            }
            var porcX = img.naturalWidth / img.width;
            var porcY = img.naturalHeight / img.height;
            $('input[name="x1"]').val(Math.round(selection.x1 * porcX));
            $('input[name="y1"]').val(Math.round(selection.y1 * porcY));
            $('input[name="x2"]').val(Math.round(selection.x2 * porcX));
            $('input[name="y2"]').val(Math.round(selection.y2 * porcY));
            $('input[name="w"]').val(Math.round(selection.width * porcX));
            $('input[name="h"]').val(Math.round(selection.height * porcY));
        }
    });
</script>
@endsection