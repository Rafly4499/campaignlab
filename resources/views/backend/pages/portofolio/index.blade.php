@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Portofolio</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Portofolio</a></li>
                <li class="breadcrumb-item active" aria-current="page">List Portofolio</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">List Portofolio</h3>
          </div>
          <div class="card-header">
                    <a href="{{ url('/admin/portofolio/create') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>  Tambah Data</button></a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                                    <th scope="col" class="sort" data-sort="name">No</th>
                                    <th scope="col" class="sort" data-sort="name">Nama Portofolio</th>
                                    <th scope="col" class="sort" data-sort="name">Kategori Portofolio</th>
                                    <th scope="col" class="sort" data-sort="name">Image</th>
                                    <th scope="col" class="sort" data-sort="name">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> 
                                @foreach($portofolio as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $item->name }}</td>
                                    
                                    <td>{{ $item->category['name'] }}
                                    </td>
                                    <td><img style="border-radius: 5px; width:200px" src="{{ url('assets/portofolio/img/'.$item->photo) }}" alt=""></td>
                                    <td>
                                        <form action="{{ url('/admin/portofolio/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="{{ url('/admin/portofolio/'.$item->id.'/edit') }}" class="btn btn-success"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <a href="{{ url('/admin/portofolio/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i> View</a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i> Delete</button>

                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
@endsection
