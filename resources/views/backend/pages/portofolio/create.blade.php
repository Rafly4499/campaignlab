@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Portfolio</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Portfolio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Create Data</h3>
          </div>

          <div class="card-body">
                    <a href="{{ url('/admin/portofolio') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" action="{{ url('/admin/portofolio/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                     
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Nama Portofolio</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Deskripsi</label>
                            <div class="col-md-12">
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="description"></textarea>
                            </div>
                        </div>
                        <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');

                        </script>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Category Portofolio</label>
                                <select class="form-control select2" name="portofolio_categories_id" id="" required>
                                    @foreach($category as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Link Portofolio</label>
                            <div class="col-md-12">
                                <input type="url" class="form-control" name="link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Gambar Portofolio</label>
                            <div class="col-md-12" style="margin-top: 10px">
                                <input type="file" class="form-control-file" name="image">
                            </div>
                        </div>
                        <button type="submit" name="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
