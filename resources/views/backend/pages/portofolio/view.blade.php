@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Portfolio</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Portfolio</a></li>
                <li class="breadcrumb-item active" aria-current="page">View</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">View Data</h3>
          </div>

          <div class="card-body">
                                <a href="{{ url('/admin/portofolio') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                               
                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Nama Portofolio</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$portofolio->name}}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Deskripsi   :</label><br>
                                                        
                                                        <div class="col-md-8"><p>{!!$portofolio->description!!}</p> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Kategori Portofolio</label>
                                                        <div class="col-md-8"> <p>:{{ $portofolio->category['name'] }}</p> 
                                                        
                                                        
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                       
                                                        <label class="col-md-4">Link URL Portofolio</label>
                                                        
                                                        <div class="col-md-8"><p>:{{$portofolio->link}}</p> </div>
                                                    </div>
                                                </div>
                                                
                                    
                                   <a href="{{ url('/admin/portofolio/'.$portofolio->id.'/edit') }}"><button type="submit" name="submit" class="btn btn-info">UPDATE</button></a> 
                                     
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                            <div class="form-group">
                                        <label for="" class="col-md-6 control-label">Gambar Portofolio</label>
                                    </div>
                                <div class="col-md-12">
                                <?php
                                    if($portofolio->photo){
                                        
                                ?>
                                <img style="border-radius: 5px; width:100%" src="{{ url('assets/portofolio/img/'.$portofolio->photo) }}" alt="">
                                <?php
                                    }else{
                                ?>
                                <h6>Belum Ada Data Gambar</h6>
                                <?php
                                    }
                                ?>
                                </div>
                            
                                    
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            @endsection