@extends('layouts.backend') 
@section('content')

<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Author</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Author</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Create Data</h3>
          </div>

          <div class="card-body">
                    <a href="{{ url('/admin/author') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form class="form-body" method="post" id="formauthor" action="{{ url('/admin/author/') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Nama author</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="nama" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Deskripsi Singkat</label>
                            <div class="col-md-12">
                                <textarea class="form-control" id="summary-ckeditor" rows="3" name="deskripsi_singkat" required></textarea>
                            </div>
                        </div>
                        <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
                        <script>
                            CKEDITOR.replace('summary-ckeditor');

                        </script>               
                        
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Email</label>
                            <div class="col-md-12">
                                <input type="email" class="form-control" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <label for="" class="col-sm-12 control-label">Link Sosmed (Isi link sesuai dengan icon)</label>
                            <div class="row">
                                <input type="hidden" name="link_sosmed[0][icon]" class="form-control" value="fab fa-linkedin">
                                <div class="col-md-2">
                                    <i style="padding:5px; font-size:20px;" class="fab fa-linkedin"></i> Linkedin
                                </div>
                                <div class="col-md-10">
                                    <input type="url" name="link_sosmed[0][url]" class="form-control" value="{{ old('link_sosmed[0][url]') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="link_sosmed[1][icon]" class="form-control" value="fab fa-instagram">
                            <div class="col-md-2">
                                <i style="padding:5px; font-size:20px;" class="fab fa-instagram"></i> Instagram
                            </div>
                            <div class="col-md-10">
                                <input type="url" name="link_sosmed[1][url]" class="form-control" value="{{ old('link_sosmed[1][url]') }}">
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Photo Author</label>
                            <div class="col-md-12" style="margin-top: 10px">
                            <div class="form-group">
                                <label for="exampleInputImage">Image:</label>
                                <h6>Gambar tidak boleh lebih dari ukuran 1000×1000 piksel</h6>
                                <input type="file" name="image" id="preview" class="image" required>
                            </div>
                        </div>
                        </div>
                        <button type="submit" class="btn btn-info">SUBMIT</button>
                        <button type="reset" class="btn btn-dark">RESET</button></a>
                        <input type="hidden" name="_method" value="post">
                    </form> 
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    
   
</script>
@endsection