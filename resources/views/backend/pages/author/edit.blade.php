@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Author</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Author</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Edit Data</h3>
          </div>

          <div class="card-body">
                    <a href="{{ url('/admin/author') }}"><button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-arrow-left"></i>  Kembali</button></a><br><br>
                    <form action="{{ url('/admin/author/'.$author->id) }}" id="formauthor" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama Author</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="nama" value="{{ $author->nama }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Deskripsi Singkat</label>
                        <div class="col-md-12">
                            <textarea class="form-control" id="summary-ckeditor" rows="3" name="deskripsi_singkat">{{ $author->deskripsi_singkat }}</textarea>
                        </div>
                    </div>
                    <script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
                    <script>
                        CKEDITOR.replace('summary-ckeditor');

                    </script>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Email</label>
                        <div class="col-md-12">
                            <input type="email" class="form-control" value="{{ $author->email }}" name="email">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        
                        <label for="" class="col-sm-12 control-label">Link Author (Isi link sesuai dengan
                            icon)</label>
                        <div class="row">
                            <input type="hidden" name="link_sosmed[0][icon]" class="form-control"
                                value="fab fa-linkedin">
                            <div class="col-md-2">
                                <i style="padding:5px; font-size:20px;" class="fab fa-linkedin"></i> Linkedin
                            </div>
                            <div class="col-md-10">
                                <input type="url" name="link_sosmed[0][url]" class="form-control"
                                    value="{{ $author->link_sosmed[0]['url'] ?? '' }}">
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" name="link_sosmed[1][icon]" class="form-control"
                                value="fab fa-instagram">
                            <div class="col-md-2">
                                <i style="padding:5px; font-size:20px;" class="fab fa-instagram"></i> Instagram
                            </div>
                            <div class="col-md-10">
                                <input type="url" name="link_sosmed[1][url]" class="form-control"
                                    value="{{ $author->link_sosmed[1]['url'] ?? '' }}">
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Photo author</label>
                            <img style="border-radius: 5px; width:120px" src="{{ url('assets/author/img/'.$author->photo) }}" alt="">
                                <div class="custom-file mt-4">
                                    <input type="file" name="image" class="custom-file-input" id="customFileLang preview" lang="en" data-default-file="{{ $author->photo }}" value="{{ $author->photo }}">
                                    <label class="custom-file-label" for="customFileLang">Select file</label>
                                </div>
                                
                               
                        </div>

                            <button type="submit" class="btn btn-info">SAVE</button>
                            <a href="{{ url('/admin/author') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection