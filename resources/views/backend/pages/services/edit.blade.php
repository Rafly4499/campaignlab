@extends('layouts.backend') 
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Layanan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}" class="text-muted">Beranda</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/admin/service') }}" class="text-muted">Service</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Edit Data</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Data Services</h4>

                    <form action="{{ url('/admin/service/'.$category->id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Service</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="{{ $category->name }}" name="name">
                            </div>
                        </div>

                        <button type="submit" name="submit" class="btn btn-info">SAVE</button>
                        <a href="{{ url('/admin/service') }}"><button type="button" class="btn btn-dark">CANCEL</button></a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
