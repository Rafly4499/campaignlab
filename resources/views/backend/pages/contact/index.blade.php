@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Contact</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Contact</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Message</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Contact Message</h3>
            <p>To reply messages, please enter the link <a href="https://mail.campaignlab.co.id/"> mail.campaignlab.co.id</a> or button reply by email cs@campaignlab.co.id</p>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                    <th scope="col" class="sort" data-sort="name">Date</th>
                                    <th scope="col" class="sort" data-sort="name">Nama Lengkap</th>
                                    <th scope="col" class="sort" data-sort="name">No Handphone</th>
                                    <th scope="col" class="sort" data-sort="name">Email</th>
                                    <th scope="col" class="sort" data-sort="name">Company</th>
                                    <th scope="col" class="sort" data-sort="name">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 0;?> @foreach($contact as $item)
                                <?php $no++;?>
                                <tr>
                                    <td>{{  date('d M Y', strtotime($item->created_at)) }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->no_hp }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->company }}</td>

                                    <td>
                                        <form action="{{ url('/admin/contact/'.$item->id) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="mailto:{{ $item->email }}" class="btn btn-success"><i class="fas fa-reply"></i> Reply</a>
                                            <a href="{{ url('/admin/contact/'.$item->id.'/view') }}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
