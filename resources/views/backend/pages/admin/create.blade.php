@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Admin</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Create Data</h3>
          </div>
          <div class="card-body">
    <form action="{{ route('admin.store') }}" method="POST">
      {{ csrf_field() }}
      <div class="row">
          <div class="col-md-2">
              <img src="{{ asset('backend/assets/img/theme/boy.png') }}" class="img-responsive img-fluid" alt="">
          </div>
          <div class="col-md-10">
              <div class="card-body">
                  @component('backend.components.form-input', [
                      'label' => 'Nama Lengkap',
                      'type' => 'text',
                      'name' => 'name',
                        'required' => TRUE,
                      'value' => old('name'),
                      'error' => $errors->first('name'),
                  ])
                  @endcomponent
                  <div class="row">
                      <div class="col-md-12">
                          @component('backend.components.form-input', [
                              'label' => 'Email',
                              'type' => 'text',
                              'name' => 'email',
                              'required' => TRUE,
                              'value' => old('email'),
                              'error' => $errors->first('email'),
                          ])
                          @endcomponent
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          @component('backend.components.form-input', [
                              'label' => 'Password',
                              'type' => 'password',
                              'name' => 'password',
                              'required' => TRUE,
                              'value' => old('password'),
                              'error' => $errors->first('password'),
                          ])
                          @endcomponent
                      </div>
                      <div class="col-md-6">
                          @component('backend.components.form-input', [
                              'label' => 'Konfirmasi Password',
                              'type' => 'password',
                              'name' => 'password_confirmation',
                              'required' => TRUE,
                              'value' => old('password_confirmation'),
                              'error' => $errors->first('password_confirmation'),
                          ])
                          @endcomponent
                      </div>
                  </div>
              </div>
              <div class="card-footer text-right">
                  <button class="btn btn-primary">Simpan</button>
              </div>
          </div>
      </div>
    </form>
  </div>
        </div>
      </div>
    </div>
  </div>
  
@endsection