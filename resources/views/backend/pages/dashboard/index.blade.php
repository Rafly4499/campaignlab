@extends('layouts.backend') 
@section('content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Admin Panel Campaign Lab</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
              <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a class="btn btn-lg btn-neutral">Campaign Lab 2021</a>
        </div>
      </div>
      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total User</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $countuser }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="ni ni-satisfied"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> Increase</span>
                <span class="text-nowrap">Since last month</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total Message</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $countcontact }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                    <i class="ni ni-single-02"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> Increase</span>
                <span class="text-nowrap">Since last month</span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card card-stats">
            <!-- Card body -->
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Total Article</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $countarticle }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-single-copy-04"></i>
                  </div>
                </div>
              </div>
              <p class="mt-3 mb-0 text-sm">
                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> Increase</span>
                <span class="text-nowrap">Since last month</span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Blog Campaign Lab</h3>
            </div>
            <div class="col text-right">
              <a href="{{ url('/admin/blog') }}" class="btn btn-sm btn-primary">See all</a>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                      <th scope="col" class="sort" data-sort="name">Title</th>
                      <th scope="col" class="sort" data-sort="name">Category</th>
                      <th scope="col" class="sort" data-sort="name">Publish</th>
                      <th scope="col" class="sort" data-sort="name">Status</th>
                  </tr>
              </thead>
              <tbody>
                  <?php $no = 0;?> @foreach($blog as $item)
                  <?php $no++;?>
                  <tr>
                    <td style="width:100px !important;">{{ Str::limit($item->title_blog, 30) }}</td>
                    <td>{{ $item->category['name_category_blog'] }}</td>
                    <td>{{ $item->published_at }}</td>
                    <td>
                      @if($item->status_draft == "true")
                      <button type="button" class="btn waves-effect waves-light btn-rounded btn-warning">Draft</button>
                      
                      @elseif($item->status_draft == "false")
                      <button type="button" class="btn waves-effect waves-light btn-rounded btn-success">Published</button>
                      
                      @endif
                    </td>
                  </tr>

                  @endforeach

              </tbody>
          </table>
      </div>
      </div>
    </div>
    <div class="col-xl-12">
      <div class="card">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Contact Messages</h3>
            </div>
            <div class="col text-right">
              <a href="{{ url('/admin/contact') }}" class="btn btn-sm btn-primary">See all</a>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                  <th scope="col" class="sort" data-sort="name">Date</th>
                      <th scope="col" class="sort" data-sort="name">Nama</th>
                      <th scope="col" class="sort" data-sort="name">Email</th>
                      <th scope="col" class="sort" data-sort="name">Perusahaan</th>
                      
                  </tr>
              </thead>
              <tbody>
                  <?php $no = 0;?> @foreach($contact as $item)
                  <?php $no++;?>
                  <tr>
                    <td>{{  date('d M Y', strtotime($item->created_at)) }}</td>
                      <td><a href="{{ url('/admin/contact/'.$item->id.'/view') }}">{{ $item->name }}</a></td>
                      <td>{{ $item->email }}</td>
                      <td>{{ $item->company }}</td>
                  </tr>

                  @endforeach

              </tbody>
          </table>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection