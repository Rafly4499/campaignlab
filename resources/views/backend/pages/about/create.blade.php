@extends('layouts.backend')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">About</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Data Identity Campaign Lab</h3>
          </div>
          <div class="card-body">
                                <a href="{{ url('/admin/about') }}"><button type="button" class="btn waves-effect waves-light btn-primary"><i class="fas fa-arrow-left"></i>  Back</button></a><br><br>
                                <form class="form-body" method="post" action="{{ url('/admin/about/') }}" enctype="multipart/form-data">
                                    @csrf 
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Vission </label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" name="vission"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">Mission </label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" name="mission"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-2 control-label">About </label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" rows="3" name="about"></textarea>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Address/Location</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="3" name="location"></textarea>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" class="form-control"/>
                                
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">Contact Person</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name="contact_person">
                                </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">No Handphone</label>
                            <div class="col-md-12">
                                <input type="number" name="no_hp" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Link Embed Map</label>
                            <div class="col-md-12">
                                <input type="url" class="form-control" name="link_embed_map">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Link Video Profile</label>
                            <div class="col-md-12">
                                <input type="url" class="form-control" name="link_video_profile">
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <label for="" class="col-sm-12 control-label">Link Sosmed (Isi link sesuai dengan icon)</label>
                            <div class="row">
                                <input type="hidden" name="link_sosmed[0][icon]" class="form-control" value="fab fa-facebook">
                                <div class="col-md-2">
                                    <i style="padding:5px; font-size:20px;" class="fab fa-facebook"></i> Facebook
                                </div>
                                <div class="col-md-10">
                                    <input type="url" name="link_sosmed[0][url]" class="form-control" value="{{ old('link_sosmed[0][url]') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="link_sosmed[1][icon]" class="form-control" value="fab fa-instagram">
                            <div class="col-md-2">
                                <i style="padding:5px; font-size:20px;" class="fab fa-instagram"></i> Instagram
                            </div>
                            <div class="col-md-10">
                                <input type="url" name="link_sosmed[1][url]" class="form-control" value="{{ old('link_sosmed[1][url]') }}">
                            </div>
                        </div>
                        </div>
                                    <button type="submit" name="submit" class="btn btn-primary">SUBMIT</button>
                                    <button type="reset" class="btn btn-dark">RESET</button></a>
                                    <input type="hidden" name="_method" value="post"> 
                                </form>
        </div>
                            </div>
                        </div>
                    </div>
                    </div>
            @endsection