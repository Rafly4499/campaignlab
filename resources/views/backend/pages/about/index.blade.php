@extends('layouts.backend')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">About</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="#">About</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data Identity</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            <a href="#" class="btn btn-sm btn-neutral">Campaign Lab</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header">
            <h3 class="mb-0">Data Identity Campaign Lab</h3>
          </div>
          <div class="card-body">
            @if($item)
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Vission</label>

                    <div class="col-md-8">
                        <p>: {!! $item->vission !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Mission</label>

                    <div class="col-md-8">
                        <p>: {!! $item->mission !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">About</label>

                    <div class="col-md-8">
                        <p>: {!! $item->about !!}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Address/Location</label>

                    <div class="col-md-8">
                        <p>: {!! $item->location !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Email</label>

                    <div class="col-md-8">
                        <div class="detblog">: {{ $item->email }}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Contact Person</label>

                    <div class="col-md-8">
                        <p>: {{ $item->contact_person }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">No HP</label>

                    <div class="col-md-8">
                        <p>: {{ $item->no_hp }}</p>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Link Embed Map</label>

                    <div class="col-md-8">
                        <p>: {{ $item->link_embed_map }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Link Video Profile</label>

                    <div class="col-md-8">
                        <p>: {{ $item->link_video_profile }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">

                    <label class="col-md-4">Link Sosmed</label>

                    <div class="col-md-8">
                        @if($item->link_sosmed)
                        @foreach ($item->link_sosmed as $link)
                        <i style="padding:5px; font-size:20px;" class="{{ $link['icon'] }}"></i> {{ $link['url'] }} <br>
                        @endforeach
                        
                        @endif
                    </div>
                </div>
            </div>

            <form action="{{ url('/admin/about/'.$item->id) }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <a href="{{ url('/admin/about/'.$item->id.'/edit') }}" class="btn btn-success"><i
                        class="fas fa-pencil-alt"></i> Edit</a>
            </form>

            @else
            <div class="card-header border-0">
            <a href="{{ url('/admin/about/create') }}"><button type="button"
                    class="btn waves-effect waves-light btn-primary"><i class="fa fa-plus"></i>Insert
                    Data</button></a>
            </div>
            @endif
        </div>
        </div>
      </div>
    </div>
  </div>
@endsection