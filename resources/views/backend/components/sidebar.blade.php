<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="{{ asset('/image/logomain.png') }}" style="max-height:65px !important;" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" href="{{ url('/admin/dashboard') }}">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            {{-- <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/about') }}">
                <i class="ni ni-planet text-primary"></i>
                <span class="nav-link-text">About</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/customer') }}">
                <i class="ni ni-single-02 text-primary"></i>
                <span class="nav-link-text">Customer</span>
              </a>
            </li> --}}
            {{-- <li class="nav-item">
              <a class="nav-link" href="profile.html">
                <i class="ni ni-single-02 text-yellow"></i>
                <span class="nav-link-text">Profile</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="tables.html">
                <i class="ni ni-bullet-list-67 text-default"></i>
                <span class="nav-link-text">Tables</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.html">
                <i class="ni ni-key-25 text-info"></i>
                <span class="nav-link-text">Login</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="register.html">
                <i class="ni ni-circle-08 text-pink"></i>
                <span class="nav-link-text">Register</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="upgrade.html">
                <i class="ni ni-send text-dark"></i>
                <span class="nav-link-text">Upgrade</span>
              </a>
            </li> --}}
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Blogs</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/blog') }}">
                <i class="ni ni-single-copy-04 text-primary"></i>
                <span class="nav-link-text">List Blog</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/categoryblog') }}">
                <i class="ni ni-money-coins text-primary"></i>
                <span class="nav-link-text">Category Blog</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/ratingblog') }}">
                <i class="ni ni-trophy text-primary"></i>
                <span class="nav-link-text">Rating Blog</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/commentblog') }}">
                <i class="ni ni-chat-round text-primary"></i>
                <span class="nav-link-text">Commentar Blog</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/author') }}">
                <i class="ni ni-badge text-primary"></i>
                <span class="nav-link-text">Author Blog</span>
              </a>
            </li>
            
          </ul>
          {{-- <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Portfolio</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/portofolio') }}">
                <i class="ni ni-single-copy-04 text-primary"></i>
                <span class="nav-link-text">List Portfolio</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/portofoliocategory') }}">
                <i class="ni ni-money-coins text-primary"></i>
                <span class="nav-link-text">Category Portfolio</span>
              </a>
            </li>

          </ul>
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Services</span>
          </h6> --}}
          <!-- Navigation -->
          {{-- <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/services') }}">
                <i class="ni ni-app text-primary"></i>
                <span class="nav-link-text">List Services</span>
              </a>
            </li>

          </ul> --}}
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Utility</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/contact') }}">
                <i class="ni ni-collection"></i>
                <span class="nav-link-text">Contact Message</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/admin/admin') }}">
                <i class="ni ni-circle-08"></i>
                <span class="nav-link-text">Admin</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active active-pro" href="{{ url('/admin/logout') }}">
                <i class="ni ni-user-run"></i>
                <span class="nav-link-text">Log Out Account</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>