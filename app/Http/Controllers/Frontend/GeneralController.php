<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Models\Portofolio;
use App\Models\PortofolioCategory;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Author;
use App\Models\Contact;
use App\Models\Quotation;
use App\Mail\QuotationEmail;
class GeneralController extends Controller
{
    public function index(){
        return view('frontend.pages.home.index'); 
    }
    public function about(){
        return view('frontend.pages.about.index'); 
    }

    public function termCondition(){
        return view('frontend.pages.terms.index'); 
    }

    public function privacyPolicy(){
        return view('frontend.pages.privacy.index');     
    }

    // Portofolio -------------------------------------------------------------------------------------------//

    public function portofolio(){
        $portofolio = Portofolio::orderBy('id','desc')->paginate(4);
        $lastportofolio = Portofolio::orderBy('id','desc')->take(5)->get();
        $category = PortofolioCategory::all();
        return view('frontend.pages.portofolio.index',compact('portofolio','category','lastportofolio'));        
    }
    public function categoryPortofolio($slug){
        if($slug){
            $category = PortofolioCategory::where('slug', $slug)->first();
            if(!$category){
                abort(404);
            }
        }
        $lastportofolio = Portofolio::orderBy('id','desc')->take(5)->get();
       
        $allcategory = PortofolioCategory::all();
        $portofolio = Portofolio::where('portofolio_categories_id','=',$category->id)->paginate(4);
        return view('frontend.pages.portofolio.category',compact('category','portofolio','lastportofolio','allcategory'));
        
    }
    public function detailPortofolio($slug){
        if($slug){
            $portofolio = Blog::where('slug', $slug)->first();
            if(!$portofolio){
                abort(404);
            }
        }
        $author = Author::where('id',$portofolio->author_id)->first();
        $category = PortofolioCategory::all();
        $lastportofolio = Portofolio::orderBy('id','desc')->take(5)->get();
        $portofolioTerkait = Portofolio::where('portofolio_categories_id', $portofolio->portofolio_categories_id)->orderBy('id','desc')->take(3)->get();
        return view('frontend.pages.portofolio.detail',compact('portofolio','category','lastportofolio','portofolioTerkait', 'author'));
                        
    }
    public function searchPortofolio(Request $request)
	{
        $lastportofolio = Portofolio::orderBy('id','desc')->take(5)->get();
        $category = PortofolioCategory::all();
		$search = $request->search;
		$portofolio = Portofolio::where('title_blog','like',"%".$search."%")->paginate(4);
		return view('frontend.pages.portofolio.search',compact('portofolio','category','lastportofolio','search'));
    }

    // Portofolio -------------------------------------------------------------------------------------------//

    public function postContact(){
           
    }
    public function getContact(){
        return view('frontend.pages.contact.index');     
    }

    //Service
    public function services(){
        return view('frontend.pages.services.index');     
    }

    public function registerConsultation(Request $request){
        $data = $request->all();
        $contact = Contact::create($data);
        Session::flash('success', 'Your Register was sent successfully. Campaign Lab will contact you shortly.');
        return redirect('/contact');
    }
    public function getQuotation()
    {
       return view('frontend.pages.quotation.index'); 
    }
    public function postQuotation(Request $request)
    {
        $validateInput = [
            'name_pic'     => 'required',
            'company'        => 'required',
            'email'          => 'required',
            'no_hp'      => 'required',
            'project'            => 'required',
            'description'            => 'required',
            'references'      => 'required',
        ];
        $data = $request->all();
            $quotation = Quotation::create($data);
            $postData = Quotation::where('id', $quotation->id)->first();
            try {
            Mail::to( 'cs@campaignlab.co.id' )->send( new QuotationEmail( $postData ) );
            Session::flash('success', 'Message sent successfully! our team will e-mail you the Creation during business hours.');
            return back();
            } catch ( \Exception $e ){
            Session::flash('error', 'Failed to send your request! please try again later');
            return back()->withInput();
            }
    }
    // Blog -------------------------------------------------------------------------------------------//

    public function blog(){
        $blog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->paginate(4);
        $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
        $category = BlogCategory::all();
        return view('frontend.pages.blog.index',compact('blog','category','lastblog'));        
    }
    public function categoryBlog($slug){
        if($slug){
            $category = BlogCategory::where('slug', $slug)->first();
            if(!$category){
                abort(404);
            }
        }
        $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
       
        $allcategory = BlogCategory::all();
        $blog = Blog::where('id_category_blog','=',$category->id)->where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
        return view('frontend.pages.blog.categoryblog',compact('category','blog','lastblog','allcategory'));
        
    }
    public function detailBlog($slug){
        if($slug){
            $blog = Blog::where('slug', $slug)->first();
            if(!$blog){
                abort(404);
            }
        }
        $author = Author::where('id',$blog->author_id)->first();
        $category = BlogCategory::all();
        $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
        $blogTerkait = Blog::where('id_category_blog', $blog->id_category_blog)->orWhere('tags','like',"%".$blog->tags."%")->orderBy('id','desc')->take(3)->get();
        return view('frontend.pages.blog.detailblog',compact('blog','category','lastblog','blogTerkait', 'author'));
                        
    }
    public function ratingblog(Request $request)
    {
    request()->validate(['rate' => 'required']);
    $post = Blog::find($request->id);
    $rating = new \willvincent\Rateable\Rating;
    $rating->rating = $request->rate;
    $post->ratings()->save($rating);
    Session::flash('success', 'Anda telah berhasil memberikan rating anda');
    return redirect()->back();
    }
    public function searchBlog(Request $request)
	{
        $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
        $category = BlogCategory::all();
		$search = $request->search;
		$blog = Blog::where('title_blog','like',"%".$search."%")->where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
		return view('frontend.pages.blog.searchblog',compact('blog','category','lastblog','search'));
    }
    public function tagBlog($slug){
        if($slug){
            $tag = Blog::Where('tags', 'like', '%' . $slug . '%')->first();
            $blog = Blog::Where('tags', 'like', '%' . $slug . '%')->paginate(4);
            if(!$blog){
                abort(404);
            }
        }
        $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
        $allcategory = BlogCategory::all();
        
        return view('frontend.pages.blog.tagblog',compact('tag','blog','lastblog','allcategory'));
        
    }

    // Blog -------------------------------------------------------------------------------------------//

}
