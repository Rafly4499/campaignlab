<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin;

class AdminController extends Controller
{

	public function index() {
		$data['data'] = Admin::all();
		return view('backend.pages.admin.index',$data);
	}

	public function create() {
		return view('backend.pages.admin.create');
	}

	public function store(Request $request)
	{
		$req = $request->all();

		if(request('password_confirmation') != request('password')){
			Session::flash('error', ' Password Tidak Sama');
			return redirect()->back();
		}
		$req['password'] = \Hash::make($req['password']);
		$result = Admin::create($req);
		Session::flash('success', 'Added Successfully');
		return redirect('admin/admin');
	}

	public function edit($id)
    {
      $data['data'] = Admin::find($id);

      return view('backend.pages.admin.edit', $data);
    }

    public function update($id, Request $request)
    {
      	$req = $request->except('_method', '_token', 'submit','password_confirmation');

        if (!empty($req['password'])) {
			if(request('password_confirmation') != request('password')){
				Session::flash('error', 'Password Tidak Sama');
				return redirect()->back();
			}
          	$req['password'] = \Hash::make($req['password']);
        }else {
          	unset($req['password']);
        }

        $result = Admin::where('id', $id)->update($req);
		Session::flash('success', 'Updated successfully');
        return redirect('admin/admin');
    }

    public function destroy($id)
    {
      $result = Admin::find($id);
      $result->delete();
	  Session::flash('success', ' Delete successfully');
      return redirect()->back();
    }
}
