<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;
class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = BlogCategory::all();
        return view('backend.pages.categoryblog.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = BlogCategory::orderBy('id')->get();
        $category = BlogCategory::all();
        return view('backend.pages.categoryblog.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
      
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'assets/categoryblog/img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
                $data['photo'] = $fileName;
                unset($data['image']);
            }
          }
        $data['slug'] = Str::slug($request->get('name_category_blog'));
    	$author = BlogCategory::create($data);
	    Session::flash('message', 'Added successfully');
	    return redirect('/admin/categoryblog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = BlogCategory::find($id);
        return view('backend.pages.categoryblog.edit', ['category' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'assets/categoryblog/img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
            $req['photo'] = $fileName;
            unset($req['image']);
    
              $result = BlogCategory::find($id);
              if (!empty($result->photo)) {
                File::delete('assets/categoryblog/img/'.$result->photo);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          $req['slug'] = Str::slug($request->get('name_category_blog'));
          $data = BlogCategory::where('id', $id)->update($req);

	    Session::flash('success', 'Updated successfully');
        return redirect('/admin/categoryblog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BlogCategory::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['name_category_blog'] . ' deleted successfully');
	    return redirect('/admin/categoryblog');
    }
}
