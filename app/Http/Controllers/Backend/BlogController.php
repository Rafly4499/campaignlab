<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Author;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Image;
use File;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::all();
        return view('backend.pages.blog.index', compact('blog'));
    }
    public function ratingblog()
    {
        $blog = Blog::all();
        return view('backend.pages.blog.rating', compact('blog'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Blog::orderBy('id')->get();
        $blog = Blog::all();
        $category = BlogCategory::all();
        $author = Author::all();
        return view('backend.pages.blog.create', compact('blog','category','author'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // var_dump($data);die();
        $data['slug'] = Str::slug($request->get('slug'));
        $tags = explode(",", $request->tags);
        
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'assets/blog/img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                $img = Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  });
                if ($request->input('w') || $request->input('h')) {
                    $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'), $request->input('x2'), $request->input('y2'));
                    $img->save($destinationPath.$fileName);
                }else{
                    $img->save($destinationPath.$fileName);
                }
                $data['photo_blog'] = $fileName;
                unset($data['image']);
            }
          }
          if($request->seturl == 'automaticurl'){
            $data['slug'] = Str::slug($request->get('title_blog'));
          }else{
            $data['slug'] = Str::slug($request->get('slug'));
          }
        if($request->setpublish == 'automatic'){
            $data['published_at'] = date('Y-m-d H:i:s');
        }else{
          $data['published_at'] = $request->published_at;
        }
        if ($request->input('status_draft') == 'true') {
          $data['status_draft'] = 'true';
        }else{
          $data['status_draft'] = 'false';
        }
    	  $blog = Blog::create($data);
        $blog->tag($tags);
        Session::flash('success', $data['title_blog'] . ' added successfully');
        return redirect('/admin/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        $category = BlogCategory::all();
        $author = Author::all();
        return view('backend.pages.blog.edit', compact('blog','category', 'author'));
    }
    public function view($id)
    {
        $blog = Blog::find($id);
        $author = Author::all();
        $category = BlogCategory::all();
        return view('backend.pages.blog.view', compact('blog','category', 'author'));
    }
    public function ratingview($id)
    {
        $blog = Blog::find($id);
        return view('backend.pages.blog.ratingview', compact('blog'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h', 'seturl', 'setpublish');
        $blog = Blog::where('id', $id)->firstorfail();
        $blog->title_blog = $request->title_blog;
        $blog->content_blog = $request->content_blog;
        $blog->id_category_blog = $request->id_category_blog;
        $blog->id_admin = $request->id_admin;
        $blog->meta_title = $request->meta_title;
        $blog->meta_description = $request->meta_description;
        $blog->author_id = $request->author_id;
      
        $tags = explode(",", $req['tags']);

        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'assets/blog/img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            $img = Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                  $constraint->aspectRatio();
                  $constraint->upsize();
              });
              if ($request->input('w') || $request->input('h')) {
                $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'), $request->input('x2'), $request->input('y2'));
                $img->save($destinationPath.$fileName);
            }else{
                $img->save($destinationPath.$fileName);
            }
            $blog->photo_blog = $fileName;
            unset($req['image']);
    
              $result = Blog::find($id);
              if (!empty($result->photo_blog)) {
                File::delete('assets/backend.pages/img/'.$result->photo_blog);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          if($request->seturl == 'automaticurl'){
            $blog->slug = Str::slug($request->get('title_blog'));
          }else{
            $blog->slug = Str::slug($request->get('slug'));
          }
          if($request->setpublish == 'automatic'){
            $blog->published_at = date('Y-m-d H:i:s');
          }elseif($request->setpublish == null){
            $blog->published_at = date('Y-m-d H:i:s');
          }else{
            $blog->published_at = $request->published_at;
          }
          if ($request->input('status_draft') == 'on') {
            $blog->status_draft = 'true';
          }else{
            $blog->status_draft = 'false';
          }
          $blog->tags = $request->tags;
          $blog->save();
          $blog->retag($tags);
	    Session::flash('success', $blog['title_blog'] . ' updated successfully');
        return redirect('admin/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Blog::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['title_blog'] . ' deleted successfully');
	    return redirect('admin/blog');
    }
}
