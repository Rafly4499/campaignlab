<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use File;

class AuthorController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author = Author::all();
        return view('backend.pages.author.index', compact('author'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Author::orderBy('id')->get();
        $author = Author::all();
        return view('backend.pages.author.create', compact('author'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
      
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $destinationPath = 'assets/author/img/'; // upload path
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renaming image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
                $data['photo'] = $fileName;
                unset($data['image']);
            }
          }

    	$author = Author::create($data);
        Session::flash('success', $data['nama'] . ' added successfully');
        return redirect('/admin/author');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Author::find($id);
       
        return view('backend.pages.author.edit', compact('author'));
    }
    public function view($id)
    {
        $author = Author::find($id);
        return view('backend.pages.author.view', compact('author'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h');
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
              $destinationPath = 'assets/author/img/'; // upload path
            $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111,99999).'.'.$extension; // renaming image
            $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
            Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
                })->save($destinationPath.$fileName);
            $req['photo'] = $fileName;
            unset($req['image']);
    
              $result = Author::find($id);
              if (!empty($result->photo)) {
                File::delete('assets/author/img/'.$result->photo);
              }
            }else {
              unset($req['image']);
            }
          }else {
            unset($req['image']);
          }
          
          $data = Author::where('id', $id)->update($req);
	    Session::flash('success', 'Updated successfully');
        return redirect('admin/author');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Author::find($id);
	    $data->destroy($id);

	    Session::flash('success', $data['nama'] . ' deleted successfully');
	    return redirect('admin/author');
    }
}
