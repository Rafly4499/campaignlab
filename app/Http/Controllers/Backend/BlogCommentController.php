<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use App\Models\BlogComment;
class BlogCommentController extends Controller
{
    public function index()
    {
        $comment = BlogComment::all();
        return view('backend.pages.blogcomment.index', compact('comment'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = BlogComment::find($id);
        return view('backend.pages.blogcomment.edit', ['comment' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = BlogComment::find($id);
        $comment = $request->all();
        $data->update($comment);

	    Session::flash('message', 'Updated status successfully');
        return redirect('/admin/commentarticle');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BlogComment::find($id);
	    $data->destroy($id);
	    Session::flash('message', 'Deleted successfully');
	    return redirect('/admin/commentarticle');
    }
}
