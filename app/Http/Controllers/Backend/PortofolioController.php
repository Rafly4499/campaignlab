<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\Portofolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\PortofolioCategory;
use Image;

use File;
class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portofolio = Portofolio::all();
        return view('backend.pages.portofolio.index', compact('portofolio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Portofolio::orderBy('id')->get();
        $portofolio = Portofolio::all();
        $category = PortofolioCategory::all();
        return view('backend.pages.portofolio.create', compact('portofolio','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
              if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $destinationPath = 'assets/portofolio/img/'; // upload path
                    $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                    $fileName = rand(11111,99999).'.'.$extension; // renaming image
                    $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                    Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                    })->save($destinationPath.$fileName);
                    $data['photo'] = $fileName;
                    unset($data['image']);
                }
              }
              $data['slug'] = Str::slug($request->get('name'));
              $portof = Portofolio::create($data);
        Session::flash('success', ' added successfully');
              return redirect('/admin/portofolio');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function show(Portofolio $portofolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portofolio=Portofolio::with('category')->FindorFail($id);
        $category=PortofolioCategory::all();
        $idcat = $portofolio->category->pluck('id');
        return view('backend.pages.portofolio.edit',['portofolio'=>$portofolio,'category'=>$category, 'idcat' => $idcat]);
    }
    public function view($id)
    {   
        $portofolio = Portofolio::find($id);
        $category = PortofolioCategory::all();
        return view('backend.pages.portofolio.view', compact('portofolio','category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $req = $request->except('_method', '_token', 'submit', 'x1', 'y1', 'x2', 'y2', 'w', 'h');
      if ($request->hasFile('image')) {
          if ($request->file('image')->isValid()) {
            $destinationPath = 'assets/portofolio/img/'; // upload path
          $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
          $fileName = rand(11111,99999).'.'.$extension; // renaming image
          $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
          Image::make($destinationPath.$fileName)->resize(500, null, function($constraint) {
              $constraint->aspectRatio();
              $constraint->upsize();
              })->save($destinationPath.$fileName);
          $req['photo'] = $fileName;
          unset($req['image']);
  
            $result = Portofolio::find($id);
            if (!empty($result->photo)) {
              File::delete('assets/portofolio/img/'.$result->photo);
            }
          }else {
            unset($req['image']);
          }
        }else {
          unset($req['image']);
        }
        $req['slug'] = Str::slug($req['name']);
        $data = Portofolio::where('id', $id)->update($req);
	    Session::flash('success', ' updated successfully');
        return redirect('admin/portofolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Portofolio  $portofolio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Portofolio::find($id);
        $data->category()->detach();
        $data->media()->delete();
        $data->destroy($id);
	    Session::flash('success', $data['name'] . ' deleted successfully');
	    return redirect('admin/portofolio');
    }
}
