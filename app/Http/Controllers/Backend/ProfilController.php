<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin;
use Auth;
class ProfilController extends Controller
{

	public function index() {
		$data['data'] = Admin::where('id',Auth::guard('admin')->user()->id)->get();
		return view('backend.pages.profil.index',$data);
	}

    public function update(Request $request)
    {
      	$req = $request->except('password_confirmation','_token');

        if (!empty($req['password'])) {
			if(request('password_confirmation') != request('password')){
        Session::flash('error', ' Password Tidak Sama');
        return redirect()->back();
			}
          	$req['password'] = \Hash::make($req['password']);
        }else {
          	unset($req['password']);
        }

        $result = Admin::where('id', Auth::guard('admin')->user()->id)->update($req);

        Session::flash('success', ' Success Successfully');
			return redirect()->back();
    }

    public function destroy($id)
    {
      $result = Admin::find($id);
      $result->delete();
      Session::flash('success', 'Deleted Successfully');
      return redirect()->back();
    }
}
