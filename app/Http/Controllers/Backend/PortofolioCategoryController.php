<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\PortofolioCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
class PortofolioCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = PortofolioCategory::all();
        return view('backend.pages.portofoliocategory.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = PortofolioCategory::orderBy('id')->get();
        $category = PortofolioCategory::all();
        return view('backend.pages.portofoliocategory.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->get('name'));
	    PortofolioCategory::create($data);
	    Session::flash('message', $data['name'] . ' added successfully');
	    return redirect('/admin/portofoliocategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PortofolioCategory  $portofolioCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PortofolioCategory  $portofolioCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = PortofolioCategory::find($id);
        return view('backend.pages.portofoliocategory.edit', ['category' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PortofolioCategory  $portofolioCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = PortofolioCategory::find($id);
        $category = $request->all();
        $category['slug'] = Str::slug($category['name']);
        $data->update($category);

	    Session::flash('success', $data['name'] . ' updated successfully');
        return redirect('/admin/portofoliocategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PortofolioCategory  $portofolioCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = PortofolioCategory::find($id);
	    $data->destroy($id);
	    Session::flash('success', $data['name'] . ' deleted successfully');
	    return redirect('/admin/portofoliocategory');
    }
}
