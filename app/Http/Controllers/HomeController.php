<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partnership;
use App\Models\User;
use App\Models\Blog;
use App\Models\FA26;
use App\Models\Colleage;
use App\Models\CategoryPublication;
use App\Models\CategoryInfo;
use App\Models\Contact;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blog = Blog::orderBy('id','desc')->limit(3)->get();
        $contact = Contact::orderBy('created_at','desc')->limit(3)->get();
        $countuser = User::all()->count();
        $countarticle = Blog::all()->count();
        $countcontact = Contact::all()->count();
        return view('backend.pages.dashboard.index', compact('blog','countuser', 'countarticle', 'countcontact','contact'));
        }
}
