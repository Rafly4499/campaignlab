<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\Author;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Image;
use File;
use Auth;
class MediaController extends Controller
{
    public function index(){
            $blog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->paginate(7);
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            // $commentblog = BlogComment::where
            $category = BlogCategory::orderBy('id','desc')->limit(5)->get();
            $allcategory = BlogCategory::all();
            $newblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(3)->get();
            return view('media.pages.blog.index',compact('blog','category','lastblog','newblog','allcategory')); 
    }
        // Blog -------------------------------------------------------------------------------------------//

        public function blog(){
            $blog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->paginate(4);
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $category = BlogCategory::all();
            return view('media.pages.blog.index',compact('blog','category','lastblog'));        
        }
        public function categoryBlog($slug){
            if($slug){
                $category = BlogCategory::where('slug', $slug)->first();
                if(!$category){
                    abort(404);
                }
            }
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $topcategory = BlogCategory::orderBy('id','desc')->limit(5)->get();
            $allcategory = BlogCategory::all();
            $blog = Blog::where('id_category_blog','=',$category->id)->where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
            return view('media.pages.blog.categoryblog',compact('category','blog','lastblog','allcategory','topcategory'));
            
        }
        public function detailBlog($slug){
            if($slug){
                $blog = Blog::where('slug', $slug)->first();
                if(!$blog){
                    abort(404);
                }
            }
            $author = Author::where('id',$blog->author_id)->first();
            $category = BlogCategory::all();
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $blogTerkait = Blog::where('id_category_blog', $blog->id_category_blog)->orWhere('tags','like',"%".$blog->tags."%")->orderBy('id','desc')->take(3)->get();
            return view('media.pages.blog.detailblog',compact('blog','category','lastblog','blogTerkait', 'author'));
                            
        }
        public function ratingblog(Request $request)
        {
        request()->validate(['rate' => 'required']);
        $post = Blog::find($request->id);
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $post->ratings()->save($rating);
        Session::flash('success', 'Anda telah berhasil memberikan rating anda');
        return redirect()->back();
        }
        public function searchBlog(Request $request)
        {
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $category = BlogCategory::all();
            $search = $request->search;
            $topcategory = BlogCategory::orderBy('id','desc')->limit(5)->get();
            $blog = Blog::where('title_blog','like',"%".$search."%")->where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->paginate(4);
            return view('media.pages.blog.searchblog',compact('blog','category','lastblog','search','topcategory'));
        }
        public function tagBlog($slug){
            if($slug){
                $tag = Blog::Where('tags', 'like', '%' . $slug . '%')->first();
                $blog = Blog::Where('tags', 'like', '%' . $slug . '%')->paginate(4);
                if(!$blog){
                    abort(404);
                }
            }
            $topcategory = BlogCategory::orderBy('id','desc')->limit(5)->get();
            $lastblog = Blog::where('status_draft','false')->where('published_at','<=',date('Y-m-d H:m',time()))->orderBy('id','desc')->take(5)->get();
            $allcategory = BlogCategory::all();
            
            return view('media.pages.blog.tagblog',compact('tag','blog','lastblog','allcategory','topcategory'));
            
        }
        
}
