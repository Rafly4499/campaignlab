<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuotationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var user
     */
    public $info;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function __construct( $user )
    {
        $this->info = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $retVal = $this;
        return $retVal
                ->subject('Message from '.$this->info['company'].'- Quotation Business')
                ->from($this->info['email'], $this->info['company'])
                ->view( 'mails.quotation', ['info' => $this->info] );
    }
}