<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortofolioCategory extends Model
{
    use HasFactory;
    protected $table = "portofolio_categories";
    protected $fillable= [
        'name', 'slug',
    ];
    protected $guarded = [];
    public function portofolio(){
        return $this->hasMany(Portofolio::class, 'portofolio_categories_id');
    }
}
