<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    use HasFactory;
    protected $table = "blog_comment";
    protected $fillable= [
        'users_id', 'id_blog','content', 'status'
    ];
    public function user(){
        return $this->belongsTo(User::class,'users_id');
    }
    public function blog(){
        return $this->belongsTo(Blog::class,'id_blog');
    }
}
