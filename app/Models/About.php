<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    protected $table = "about";
    protected $fillable= [
        'location', 'vission', 'mission', 'about', 'catalog', 'email', 'contact_person', 'no_hp', 'link_embed_map', 'link_video_profile', 'link_sosmed'
    ];
    protected $casts = ['link_sosmed' => 'array'];
    protected $appends = ['link_sosmed'];
    public function getLinksAttribute(){
        return json_decode($this->attributes['link_sosmed']);
    }
}
