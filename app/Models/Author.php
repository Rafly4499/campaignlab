<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    protected $table = "author";
    protected $guarded = [];
    protected $fillable= [
        'nama', 'deskripsi_singkat', 'photo', 'link_sosmed', 'email' 
    ];
    protected $casts = ['link_sosmed' => 'array'];
    protected $appends = ['link_sosmed'];
    public function getLinksAttribute(){
        return json_decode($this->attributes['link_sosmed']);
    }
    public function blog(){
        return $this->hasMany(Blog::class, 'author_id');
    }
}
