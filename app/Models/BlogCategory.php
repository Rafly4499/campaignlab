<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use HasFactory;
    protected $table = "blog_category";

    protected $guarded = [];
    protected $fillable= [
        'name_category_blog', 'photo', 'slug',
    ];
    public function blog(){
        return $this->hasMany(Blog::class, 'id_category_blog');
    }
}
