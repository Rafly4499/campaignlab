<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = "quotation";
    protected $fillable= [
        'name_pic', 'company', 'email', 'no_hp', 'project', 'description', 'references'
    ];
}
