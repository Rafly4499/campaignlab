<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Blog extends Model
{
    use HasFactory;
    protected $table = "blog";
    protected $guarded = [];
    protected $fillable= [
        'title_blog', 'content_blog', 'photo_blog', 'tgl_posting', 'slug', 'tags', 'author_id', 'id_admin', 'id_category_blog', 'meta_title', 'meta_description', 'published_at', 'status_draft'
    ];
    use Rateable;
    use \Conner\Tagging\Taggable;

    public function category(){
        return $this->belongsTo(BlogCategory::class,'id_category_blog');
    }
    public function author(){
        return $this->belongsTo(Author::class,'author_id');
    }
}
