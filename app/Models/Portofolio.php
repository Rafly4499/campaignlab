<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "portofolio";
    protected $fillable= [
        'name', 'description', 'link', 'slug', 'photo', 'portofolio_categories_id'
    ];
    public function category(){
        return $this->belongsTo(PortofolioCategory::class,'portofolio_categories_id');
    }
}
